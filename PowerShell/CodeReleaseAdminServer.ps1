
param($ReleaseOption = "",
	  $RestartWebServers = "")

$RunLocally = "0" #Parameter to pass to script on remote sever so it doesnt log locally

# Ask for Load startup parameters if not set on load
if ($ReleaseOption.length -eq 0)
	{
		write-host "Which Site do you want to release: `r`n 1 - Studio `r`n 2 - Sites `r`n 3 - Both `r`n 4 - Just 3p `r`n" -foregroundcolor "yellow"
		$ReleaseOption = read-host ":" # 1 - Studio, 2 - Sites, 3 - Both, 4 - Just 3p
	}

if ($RestartWebServers.length -eq 0)
	{
		write-host "Restart Web Servers (Yes/No)" -foregroundcolor "yellow"
		$RestartWebServers = read-host ":" #Yes, No, Y, N
	}

write-output $RestartWebServers	
	
#Load the XML Configuration File
$path = "C:\AdminScripts\ReleaseConfig.xml"
$LogFile = "C:\AdminScripts\ReleaseLog.txt" 

# Reset/Create the Log File
if (!(Test-Path $LogFile))
	{
		New-Item $LogFile  -type file
	}
else
	{
		Remove-Item $LogFile
		New-Item $LogFile  -type file
	}

# Start Logging
Start-Transcript -Pat $LogFile -Append 

$CurrentPath = get-location
$xml = New-object -TypeName XML
$xml.load($path)

write-host $RestartWebServers
foreach ($servername in $xml.release.webservers.Servername)
	{
		write-host "Releasing code to " $servername -foregroundcolor "yellow"
		$session = new-psSession -computername $servername
		invoke-command -session $session -scriptblock{param($ReleaseOption,$RestartWebServers, $RunLocally) c:\adminscripts\release.ps1 -ReleaseOption $ReleaseOption -RestartWebServers $RestartWebServers -RunLocally $RunLocally} -ArgumentList ($ReleaseOption, $RestartWebServers, $RunLocally)
		remove-Pssession -ComputerName $servername
	}
	
start-sleep -s 15

Stop-Transcript 
	
