﻿

$groups = get-localgroup
$users = get-localUser 

$user_to_groups = @{}

foreach ($user in $users) {
    $user_to_groups.Set_Item($user.Name,@())
}

foreach ($group in $groups ) {
  $users_in_group = Get-LocalGroupMember -Name $group | select -Property Name

  foreach ($user in $users_in_group) {
    $total_length = $user.Name.Length
    $start_length = $user.Name.lastIndexOf('\')
    $difference = $total_length - $start_length
    
    $extracted_user_name = $user.Name.Substring($start_length+1,$difference -1)    

    $user_to_groups.$extracted_user_name += $group.Name 
  } 
}

write-output $user_to_groups 