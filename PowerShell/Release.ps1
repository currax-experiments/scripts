<#
#>
param($ReleaseOption = "",
	  $RestartWebServers = "",
	  $RunLocally ="" )

# Ask for Load startup parameters if not set on load
if ($ReleaseOption.length -eq 0)
	{
		write-host "Which Site do you want to release: `r`n 1 - Studio `r`n 2 - Sites `r`n 3 - Both `r`n 4 - Just 3p `r`n" -foregroundcolor "yellow"
		$ReleaseOption = read-host ":" # 1 - Studio, 2 - Sites, 3 - Both, 4 - Just 3p
	}

if ($RestartWebServers.length -eq 0)
	{
		write-host "Restart Web Servers (Yes/No)" -foregroundcolor "yellow"
		$RestartWebServers = read-host ":" #Yes, No, Y, N
	}

if ($RunLocally.length -eq 0)
	{
		$RunLocally = "1"
	}


#Configuration and Log file
$path = "C:\Users\daniel.chernoff\Documents\_Scripts\PowerShell\ReleaseConfig.xml"
$LogFile = "C:\Users\daniel.chernoff\Documents\_Scripts\PowerShell\ReleaseLog.txt" 

$CurrentPath = get-location
$xml = New-object -TypeName XML
$xml.load($path)
$apacheservice = $xml.release.apacheservice

#Load Studio Property information
	$path = @{name='path'; expression = {$_.info.path}}
	$service = @{name='service'; expression = {$_.info.service}}
	$item = select-xml -xml $xml -XPath '//website[name="studio"]'
	
	$Studio = $item.node | select-object -Property name, $path, $service

	rv item
	rv path
	rv service

#Load Sites/Member Connect Property information
	$path = @{name='path'; expression = {$_.info.path}}
	$service = @{name='service'; expression = {$_.info.service}}
	$item = select-xml -xml $xml -XPath '//website[name="sites"]'

	$sites = $item.node | select-object -Property name, $path, $service

	rv item
	rv path
	rv service

#Load 3p Property information
	$path = @{name='path'; expression = {$_.info.path}}
	$service = @{name='service'; expression = {$_.info.service}}
	$item = select-xml -xml $xml -XPath '//website[name="3p"]'

	$3p = $item.node | select-object -Property name, $path, $service

	rv item
	rv path
	rv service

# Reset/Create the Log File
if ($RunLocally -eq 1)
	{
		if (!(Test-Path $LogFile))
			{
				New-Item $LogFile  -type file
			}
		else
			{
				Remove-Item $LogFile
				New-Item $LogFile  -type file
			}
		
		# Start Logging
		Start-Transcript -Pat $LogFile -Append 
	}



# ------------------------  Code that does the work -----------------------------------#
$tmp = $RestartWebServers.ToUpper( )
if ($tmp.StartsWith("Y"))
	{
		write-host  'Stopping Web Services' -foregroundcolor "green"
		stop-service $apacheService
		stop-service $studio.service
		stop-service $sites.service
	}
write-host 'Updating Websites' -foregroundcolor "green"
switch ($ReleaseOption)
	{
		1 #Only update studio code
			{
			write-host 'Updating Studio' -foregroundcolor "green"
			set-location $studio.path
			git reset --hard
			git pull origin master	--quiet		
			}
		2 #only update sites code
			{
			write-host 'Updating Sites' -foregroundcolor "green"
			set-location $sites.path
			git reset --hard
			git pull origin master --quiet
			}
		3 #update sites and studio code
			{
			write-host 'Updating Studio' -foregroundcolor "green"
			set-location $studio.path
			git reset --hard
			git pull origin master --quiet
			
			write-host 'Updating Sites' -foregroundcolor "green"
			set-location $sites.path
			git reset --hard
			git pull origin master --quiet
			}
	}

write-host 'Updating 3p' -foregroundcolor "green"
# set-location $3p.path
git pull origin master --quiet



$tmp = $RestartWebServers.ToUpper( )
if ($tmp.StartsWith("Y"))
	{
		write-host 'Starting Web Services' -foregroundcolor "Green"
		start-service $studio.service
		start-service $sites.service
		start-service $apacheService
	}

set-location $CurrentPath
if ($RunLocally -eq 1)
	{Stop-Transcript }




