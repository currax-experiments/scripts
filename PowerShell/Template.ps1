﻿#REQUIRES -Version 3.0
<#
.SYNOPSIS
	Lorem Ipsum

.DESCRIPTION
    Lorem Ipsum

.PARAMETER <ParameterName>
	Gloria in exclesis Deo.

.PARAMETER <ParameterName>
	Confiteor Deo Omnipotenti

.PARAMETER <ParameterName>
	beate marie semper virgnini

.PARAMETER <ParameterName>
	beato michaeli archangelo

	
.Notes
	File Name      : <Name of the script>
	Author         : Kilmore
	Prerequisite   : Powershell 3.0
	Version        : 1.0

	Possible Enhancements
#>

param(
	[String]$ParameterName1 = "", 
	[String] $ParameterName2 = "0",
	[String] $ParameterName3 = "",
    [String] $ParameterName4 = ""
	)


# -------------------------------------------------------------------------  Pre Processing ------------------



# -------------------------------------------------------------------------  Load Modules ------------------



# -------------------------------------------------------------------------  Functions ------------------



# -------------------------------------------------------------------------   Main ------------------



<#
************************************************************************************************************
Code Snippets
************************************************************************************************************s
#>


<#
#  --------------------- accept insecure certificates
# This is used to accept any certificates (e.g. mismatches with domain names, self signed certs, etc)
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

#>

<# -------------------- Load SQL PS moduels
<#  # SQL Server Commandlet Snapin
    # Set this in the Modules loading when using invoke-SQLCMD

if ( (Get-PSSnapin -Name SqlServerCmdletSnapin100 -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin SqlServerCmdletSnapin100
}

if ( (Get-PSSnapin -Name SqlServerProviderSnapin100 -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin SqlServerProviderSnapin100
}


# Run a query
  $Query = @"
    select * from table a where a.stuff like "This"
    "@
  
  $QueryResult = (invoke-sqlcmd -Query $query -serverinstance $servername -database $database)

#>


