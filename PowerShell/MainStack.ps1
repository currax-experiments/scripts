﻿$JSONFile  = "c:\downloads\MainStackTest.json"
$CSVFile = "c:\downloads\MainStackTest.csv"
$D = ','
$E = """"

If (Test-Path $csvfile){
	Remove-Item $csvfile
}


$x = (Get-Content $JSONFile) -join "`n" | ConvertFrom-Json

$HeaderRow = "customerid,CreditcardID,Number,Name,exp_month,exp_year,address_line1,address_line2,address_city,address_state,address_zip,Address_country"
$HeaderRow | Add-content $CSVFile 

$row = ""
foreach($customer in $x.customers){
    $row += $E + $customer.id + $E + $D
    foreach($card in $customer.cards){
    $row += $E + $card.id + $E + $D
    $row += $E + $card.Number + $E + $D
    $row += $E + $card.Name + $E + $D
    $row += $E + $card.exp_month + $E + $D
    $row += $E + $card.exp_year + $E + $D
    $row += $E + $card.address_line1 + $E + $D
    $row += $E + $card.address_line2 + $E + $D
    $row += $E + $card.address_city + $E + $D
    $row += $E + $card.address_state + $E + $D
    $row += $E + $card.address_zip + $E + $D
    $row += $E + $card.address_country + $E
    }

 write-output $row 
 $row | add-content $CSVFile
 $row = ""
}


