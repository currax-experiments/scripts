USE [zenplanner-office]
GO

/****** Object:  UserDefinedFunction [dbo].[ConvertHHMMSS2Seconds]    Script Date: 07/02/2013 14:09:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE function [dbo].[ConvertHHMMSS2Seconds](@TimeString varchar(max))
returns int
begin
declare @TimeInSeconds int
set @TimeInSeconds = '0'

set @TimeInSeconds =	
   case 
		when (LEN(@TimeString) - LEN(REPLACE(@TimeString,':', '')))/LEN(':') = 2 
			then 
			   cast((LEFT(@TimeString,charindex(':',@TimeString,1)-1)) as int)*3600
			   + CAST((SUBSTRING(@TimeString,charindex(':',@TimeString,1)+1,2)) as int) * 60
			   + CAST(RIGHT(@TimeString,2) as int)

		when (LEN(@TimeString) - LEN(REPLACE(@TimeString,':', '')))/LEN(':') = 1
			then 
			  cast((LEFT(@TimeString,charindex(':',@TimeString,1)-1)) as int)*60 
			  + CAST((SUBSTRING(@TimeString,charindex(':',@TimeString,1)+1,2)) as int)
	    
	    else LEN(@TimeString)
	end 

return @TimeInSeconds

end
GO


