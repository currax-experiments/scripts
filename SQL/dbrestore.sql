/* 
	 Assumes no FK against tables being truncated
	 Still to do
		1. Secure xp_cmdshell
		2. Assumes one file for data one file for log
*/

/*  Declare ------------------------------------------ */
declare @DBToRestore	nvarchar(50)
declare @DBRestoreTo	nvarchar(50)
declare @FileName		nvarchar(200)
declare @FilePath		nvarchar(100)
declare @FilePathData	nvarchar(100)
declare @FileDate		nvarchar(50)
declare @DirContents	Table(Contents varchar(100))
declare @Command		varchar(5000)
declare @TablesToTrunc	varchar(max)
declare @DataLogicalFileName varchar(100)
declare @LogLogicalFileName	 varchar(100)
declare @fileListTable table -- Used to determine the logical file name
(
    LogicalName          nvarchar(128),
    PhysicalName         nvarchar(260),
    [Type]               char(1),
    FileGroupName        nvarchar(128),
    Size                 numeric(20,0),
    MaxSize              numeric(20,0),
    FileID               bigint,
    CreateLSN            numeric(25,0),
    DropLSN              numeric(25,0),
    UniqueID             uniqueidentifier,
    ReadOnlyLSN          numeric(25,0),
    ReadWriteLSN         numeric(25,0),
    BackupSizeInBytes    bigint,
    SourceBlockSize      int,
    FileGroupID          int,
    LogGroupGUID         uniqueidentifier,
    DifferentialBaseLSN  numeric(25,0),
    DifferentialBaseGUID uniqueidentifier,
    IsReadOnl            bit,
    IsPresent            bit,
    TDEThumbprint        varbinary(32)
)

/* Defautl Values ------------------------------------- */
set @DBToRestore  = 'ZenPlanner-Production'
set @DBRestoreTo = 'ZenPlanner-Restore'
set @FilePath = 'c:\mssql\backups\' + @DBToRestore + '\'
Set @FilePathData = 'c:\mssql\Data\'
Set @Command = 'dir ' + @FilePath


/* Get File to restore if one not specified ------------------ */
Print 'Getting Directory Contents' + char(13)
INSERT INTO @DirContents
EXEC xp_cmdshell @Command 
set @FileName = @FilePath + rtrim((
				select top 1 BackupFileName
				from 
					(select	cast(SUBSTRING(contents,1,20) as datetime) as BackupDate,
							substring(contents,CHARINDEX(@DBToRestore,Contents),CHARINDEX('bak',contents)-CHARINDEX(@DBToRestore,Contents)+3) as BackupFileName
					 from @dircontents where contents like '%.bak') as FL
				Order By BackupDate DESC))
Print @FileName + char(13)

/* Determine Logical File Names for restored database ---------- */

set @Command = 'restore filelistonly from disk= ''' + @FileName + ''''
Print @Command
insert into @fileListTable
exec(@command)

Set @DataLogicalFileName = (select top 1 LogicalName from @fileListTable where Type = 'D')
Print 'Data File:' + @DataLogicalFileName + char(13)
set @LogLogicalFileName = (select top 1 LogicalName from @fileListTable where Type = 'L')
Print 'Log File:' + @LogLogicalFileName + char(13)

/* Drop Restore Datbase if it exists --------------------------- */
IF EXISTS (select name from sys.databases where name = @DBRestoreTo)
	Begin
	/* Put DB in single user mode, to kill connections */
	set @Command = 'use master 
					alter database [' + @DBRestoreTo + '] set single_user with rollback immediate;'
	print @Command + char(13)
	exec (@Command )
	
	/*Drop the database */
	set @Command = 'Drop Database [' + @DBRestoreTo + ']'
	print @Command + char(13)
	exec (@Command)
	END
/* Create Restore Database ------------------------------------- */
Print 'Creating Database' + char(13)
set @Command = 'CREATE DATABASE [ZenPlanner-Restore]		
				ON 
					(NAME = [ZenPlanner-Restore],
					FILENAME = ''' + @FilePathData + @DBRestoreTo + '.mdf'',
					SIZE = 10MB)
				LOG ON
				(NAME = [ZenPlanner-Restore_log],
					FILENAME = ''' + @FilePathData + @DBRestoreTo + '.ldf'',
				SIZE = 5MB)'
print @command + char(13);
Exec (@Command)
/* Restore Database --------------------------------------------- */
set @Command = 'Restore DATABASE [' + @DBRestoreTo + '] from DISK = ''' + @FileName + '''
				WITH REPLACE,
			    Recovery,
				MOVE ''' + @DataLogicalFileName + ''' TO '''+ @FilePathData + @DBRestoreTo + '.mdf'',
				MOVE ''' + @LogLogicalFileName + ''' TO '''+ @FilePathData + @DBRestoreTo + '.ldf'''

print @command + char(13);
exec (@Command)

/* Change the recovery model ---------------------------------------- */
set @Command = 'ALTER DATABASE [' + @DBRestoreto + '] SET RECOVERY SIMPLE WITH NO_WAIT'
print @command + char(13);
exec (@Command)


/* Truncate Tables --------------------------------------------------- */
set @Command = 'use [' + @DBRestoreto + ']  TRUNCATE TABLE email;'
print @Command + char(13);
exec (@command)


set @Command = 'use [' + @DBRestoreto + ']  TRUNCATE TABLE log;'
print @Command + char(13);
exec (@command)


/* Shrink the ZenPlanner  -------------------------------------------- */
set @command = 'DBCC SHRINKDATABASE ([' + @DBRestoreTo + '], 10);'
print @Command + char(13);

