USE [ZenPlanner-Management]
GO

/****** Object:  StoredProcedure [dbo].[PerformanceCapturePerfMonData]    Script Date: 9/8/2015 1:44:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[PerformanceCapturePerfMonData]
						@CaptureDateTime DateTime = Null
AS

SET NOCOUNT ON;

DECLARE @PerfCounters TABLE
    (
      [Counter] NVARCHAR(770) ,
      [CounterType] INT ,
      [FirstValue] DECIMAL(38, 2) ,
      [FirstDateTime] DATETIME ,
      [SecondValue] DECIMAL(38, 2) ,
      [SecondDateTime] DATETIME ,
      [ValueDiff] AS ( [SecondValue] - [FirstValue] ) ,
      [TimeDiff] AS ( DATEDIFF(SS, FirstDateTime, SecondDateTime) ) ,
      [CounterValue] DECIMAL(38, 2)
    );

/* ----------------------------------------    Per Second Counters 
	"per-second counters" are cumulative and thus need to have a second capture in order to calculate
	the rate for the sample
*/

INSERT  INTO @PerfCounters
        ( [Counter] ,
          [CounterType] ,
          [FirstValue] ,
          [FirstDateTime]
        )
        SELECT  RTRIM([object_name]) + N':' + RTRIM([counter_name]) + N':'
                + RTRIM([instance_name]) ,
                [cntr_type] ,
                [cntr_value] ,
                GETDATE()
        FROM    sys.dm_os_performance_counters
		WHERE   cntr_type = 272696576
        ORDER BY [object_name] + N':' + [counter_name] + N':'
                + [instance_name];

WAITFOR DELAY '00:00:05';

UPDATE  @PerfCounters
SET     [SecondValue] = [cntr_value] ,
        [SecondDateTime] = GETDATE()
FROM    sys.dm_os_performance_counters
WHERE   [Counter] = RTRIM([object_name]) + N':' + RTRIM([counter_name]) + N':' + RTRIM([instance_name]);

UPDATE  @PerfCounters
SET     [CounterValue] = [ValueDiff] / [TimeDiff]
WHERE   [CounterType] = 272696576;


/* ----------------------------------------  Absolute Counters */
INSERT  INTO @PerfCounters
        ( [Counter] ,
          [CounterType] ,
          [CounterValue]
        )
SELECT  RTRIM([object_name]) + N':' + RTRIM([counter_name]) + N':' + RTRIM([instance_name]) ,
        [cntr_type] ,
        [cntr_value]
FROM    sys.dm_os_performance_counters
WHERE   cntr_type IN (65536, 65792)

/* ---------------------------------------- Bulk Counters 
    Number of items processed, on average, during an operation. 
	This counter type displays a ratio of the items processed (such as bytes sent) to the number of operations completed, 
	and requires a base property 

*/
INSERT  INTO @PerfCounters
        ( [Counter] ,
          [CounterType] ,
          [CounterValue]
        )
Select 
	  RTRIM(m.[object_name]) + N':' + RTRIM(m.[counter_name]) + N':' + RTRIM(m.[instance_name]), 
	   M.cntr_type,
	   cntr_value =
	   case 
	    when n.cntr_value > 0 then round(cast(m.cntr_value as float)/cast(n.cntr_value as float),2) 
		else '0'
	   end 
from 
	(
	select *
	from sys.dm_os_performance_counters 
	where counter_name NOT like '% base%'
		and cntr_type in ('537003264' , '1073874176')
	) M 

	inner join 
	(
	select *
	from sys.dm_os_performance_counters
	where counter_name like '% base%'
		and cntr_type in ('1073939712')
	) N on M.instance_name = N.instance_name 
			and M.counter_name = rtrim(substring(N.counter_name,1,charindex('base',N.counter_name,1)-2))
			and M.object_name = N.object_name

IF @CaptureDateTime IS NULL 
	BEGIN
		SET @CaptureDateTime = GETDATE()
	END

INSERT  INTO [dbo].[PerformancePerfMonData]
        ( [Counter] ,
          [Value] ,
          [CaptureDate]
        )
        SELECT  [Counter] ,
                [CounterValue] ,
                @CaptureDateTime 
        FROM    @PerfCounters;

GO


