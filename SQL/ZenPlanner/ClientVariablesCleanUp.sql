use [ZenPlanner-ClientVariables]
DECLARE @today varchar(200), 
        @Yesterday as varchar(200), 
		@DayBefore as varchar(200), 
		@OtherToday varchar(200), 
		@CountBefore as Int, 
		@CountAfter as Int,
		@LogsDeleted as Int

SET @Today = rtrim(CAST('LASTACCESSDATE'':createdatetime( 'AS CHAR)) 
             + rtrim(cast(DATEPART(yyyy,getdate()) as CHAR)) 
             + rtrim(cast(',' as CHAR))
			 + rtrim(cast(DATEPART(mm,getdate()) as CHAR))
			 + rtrim(cast(',' as CHAR))
			 + rtrim(cast(DATEPART(dd,getdate()) as CHAR))

SET @Yesterday = rtrim(CAST('LASTACCESSDATE'':createdatetime( 'AS CHAR))
             + rtrim(cast(DATEPART(yyyy,getdate()) as CHAR))
			 + rtrim(cast(',' as CHAR))
			 + rtrim(cast(DATEPART(mm,getdate()) as CHAR))
			 + rtrim(cast(',' as CHAR))
			 + rtrim(cast(DATEPART(dd,getdate()) - 1  as CHAR))

SET @DayBefore = rtrim(CAST('LASTACCESSDATE'':createdatetime( 'AS CHAR))
             + rtrim(cast(DATEPART(yyyy,getdate()) as CHAR))
			 + rtrim(cast(',' as CHAR))
			 + rtrim(cast(DATEPART(mm,getdate()) as CHAR))
			 + rtrim(cast(',' as CHAR))
			 + rtrim(cast(DATEPART(dd,getdate()) - 2  as CHAR))
			 
-- Get total records before delete
SET @CountBefore = (select COUNT(*) from railo_client_data)

-- Delete the un-needed client variable records
-- DELETE
SELECT *
FROM railo_client_data 
WHERE railo_client_data.data NOT LIKE '%' + @Today + '%' AND
      railo_client_data.data NOT LIKE '%' + @Yesterday + '%' AND 
	  railo_client_data.data NOT LIKE '%' + @DayBefore + '%' AND
	  railo_client_data.name NOT IN ('G','S','P')

-- Get total records after delete
Set @CountAfter = (select COUNT(*) from railo_client_data)

-- Evaluate to make sure delete didn't fail
IF @CountAfter < @CountBefore
  Begin
   Set @LogsDeleted = @CountBefore - @CountAfter
   use [ZenPlanner-ClientVariables]insert into CleanUpLog (LogMessage,DateTimeS) Values(rtrim(cast(@LogsDeleted as CHAR)) + ' Logs Records Deleted',getdate())
  End
ELSE
  Begin
   use [ZenPlanner-ClientVariables]insert into CleanUpLog (LogMessage,DateTimeS) Values('No Logs Records Deleted',getdate())
   EXEC sp_send_dbmail @profile_name='SQL Email', @recipients='dan@zenplanner.com,ben@zenplanner.com', @subject='Client Variable Log Truncate Failure', @body=''
  End
 
 -- Run Index Rebuild
ALTER INDEX [IX_railo_client_data] ON [dbo].[railo_client_data] REBUILD PARTITION = ALL WITH ( PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, ONLINE = OFF, SORT_IN_TEMPDB = OFF )  
