Use [ZenPlanner-Restore] 
/*
go
/* Alter non-zenplanner email addresses so email will not go to customers */
update Person 
set PrimaryEmail = replace(replace(replace(Replace(REPLACE(PrimaryEmail,'com','aaa'),'net','bbb'),'org','ccc'),'ca','dd'),'au','ee')
where PrimaryEmail not like '%zenplanner.com%'
		and PrimaryEmail is not null
go
/* Alter Username to match primary email */
alter table Useraccount alter column Username varchar(51)


update UserAccount 
set UserName = replace(replace(replace(Replace(REPLACE(UserName,'com','aa'),'net','bb'),'org','cc'),'ca','dd'),'au','ee')
where UserName not like '%zenplanner.com%'
		and UserName is not null

/* Get rid of sensative credit card data */
go
update creditcard 
set encryptedDigits = '', token = '', visibledigits = '111'
go

/* Remove Credit Card Gateway username and passwords */
delete from objectproperty 
where propertyname in ('cc_username','eft_username','cc_password','eft_password','cc_merchantid','eft_merchantID')

update objectproperty set propertyvalue = 'test_approved' 
where propertyName in ('eft_component','cc_component')

/* Remove old email,  message queue, log data */
Truncate table MessageQueue
Truncate table MessageQueueHistory
Truncate table log
Truncate table logautomation 
Truncate table email
Truncate table emailstatus

*/

/* Change the db name to ZenPlanner -QA */
ALTER DATABASE [zenplanner-Restore] MODIFY FILE (NAME=N'ZenPlanner-Production', NEWNAME=N'ZenPlanner-QA')
ALTER DATABASE [zenplanner-Restore] MODIFY FILE (NAME=N'ZenPlanner-Production_log', NEWNAME=N'ZenPlanner-QA_log')

/* Backup the DB so we can shrink the transaction log */
backup database [zenplanner-restore] to disk = 'F:\MSSQL\Backup\ZenPlanner-Prep.bak' with format;
backup log [zenplanner-Restore] TO disk = 'F:\MSSQL\Backup\ZenPlanner-Prep.trn'
dbcc shrinkfile ([ZenPlanner-QA_log],1)

/* Set Recovery Mode on QA DB to Simple */

ALTER DATABASE [zenplanner-Restore] SET RECOVERY SIMPLE
