select [First name] as personFirstName, 
	   [Last name] as personlastname,
	   [First name] + ' ' + [Last name] as Accountname,
	   [First name] as AccountFirstName, 
	   [Last name] as AccountLastName,
	   GTCMA.Address as BillingAddress1,
	   '' as BillingAddress2,
	   GTCMA.City as BillingCity,
	   GTCMA.State as BillingState,
	   GTCMA.[Postal code] as BillingZip,
	   'USA' as BillingCountry,
	   '' as AccountType,
	   CreditCardNo as CreditCardNumber,
	   LEN(RTRIM(ltrim(ExpMonth))) as lenghtofexpmonth,
	   CreditCardMonth = 
		Case
			-- when LEN(RTRIM(ltrim(ExpMonth))) = 0 Then '0' + ExpMonth
			when LEN(RTRIM(ltrim(ExpMonth))) = 1 Then convert(varchar(2),'0') + convert(varchar(2),ExpMonth)
			when LEN(RTRIM(ltrim(ExpMonth))) = 2 Then convert(varchar(2),ExpMonth)
		end,
	   -- ExpMonth as CreditCardMonth,
	   ExpYear as CreditCardYear,
	   ABANumber = 
		case
			when ACHRoutingNum = 'NULL' then ''
			else ACHRoutingNum 
		end,
	   AccountNumber =
		case
			when ACHAccountNum = 'NULL' then ''
			else ACHAccountNum
		end
	  
from GoodTimesCrossfitMissingAddresses GTCMA 
		inner join GoodTimesFull GTF on (GTCMA.[First name] = GTF.FirstName AND GTCMA.[Last name] = GTF.LastName)

		