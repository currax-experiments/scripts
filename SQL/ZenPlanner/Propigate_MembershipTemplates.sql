/*
	.date: 
		4-Nov-2013
		
	.summary:
		Push membership options to a subscribers school from a publisher school.
	
	.need_to_add 
		Income category is not handled here. Next time this script is used validation that an income category exists
		for the subscriber school based on the MembershipTemplate.IncomeCategoryID column. This references the 
		lookupcode.lookupcodeID column. 
*/
go

Declare @RunThisScript int = '0'

Declare @TemplatePartitionID  uniqueidentifier = 'F96DA86A-FBF3-4F5B-BC6C-F1DC6493743D'
Declare @TemplateProgramID	uniqueidentifier = '497632B1-CCEF-41F6-ABA5-7CA965F74D3D'

Declare @TampaPartitionID  uniqueidentifier = '3C4F163F-BA9C-46A6-86A5-4D247C3AE333'
Declare @TampaProgramID	uniqueidentifier = newID()

DECLARE @Tmp_Program Table(
							ProgramId uniqueidentifier,
							[PartitionId] [uniqueidentifier] NOT NULL,
							[Name] varchar(50) NOT NULL,
							[Description] [text] NULL,
							[CreateDate] [datetime] NULL,
							[Custom01] Varchar(200) NULL,
							[Custom02] Varchar(200) NULL,
							[Custom03] Varchar(200) NULL,
							[Custom04] Varchar(200) NULL,
							[Custom05] Varchar(200) NULL,
							[IncomeCategoryId] [uniqueidentifier] NULL,
							[Sort] [smallint] NULL,
							[isRanked] [bit] NULL,
							[SystemValue] [varchar](20) NULL,
							[isProspectForm] [bit] NOT NULL,
							[OldProgramID] uniqueidentifier
							)

DECLARE @Tmp_MembershipTemplate Table(
	[MembershipTemplateId] [uniqueidentifier] NOT NULL,
	[PartitionId] [uniqueidentifier] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [text] NULL,
	[BeginType] [varchar](10) NOT NULL,
	[BeginDate] [smalldatetime] NULL,
	[DurationCount] [int] NOT NULL,
	[DurationUnits] [char](1) NOT NULL,
	[AttendanceMaximumDuration] [varchar](3) NOT NULL,
	[AttendanceMaximum] [int] NULL,
	[isAutoRenew] [int] NOT NULL,
	[PaymentFrequency] [varchar](10) NOT NULL,
	[PaymentCount] [int] NOT NULL,
	[PaymentAmount] [money] NOT NULL,
	[isTaxable] [bit] NOT NULL,
	[IncomeCategoryId] [uniqueidentifier] NULL,
	[Sort] [int] NOT NULL,
	[NotifyStaffId] [uniqueidentifier] NULL,
	[isShowOnPortal] [bit] NULL,
	[Category] [varchar](20) NULL,
	[PaymentType] [varchar](10) NOT NULL,
	[DueAtSignup] [varchar](20) NULL,
	[EnrollmentBeginDate] [smalldatetime] NULL,
	[EnrollmentEndDate] [smalldatetime] NULL,
	[FirstPaymentAmount] [money] NOT NULL,
	[Custom01] [varchar](50) NULL,
	[Custom02] [varchar](50) NULL,
	[Custom03] [varchar](50) NULL,
	[Custom04] [varchar](50) NULL,
	[isTaxable2] [bit] NOT NULL,
	[AutoRenewId] [uniqueidentifier] NULL,
	[Custom05] [varchar](50) NULL,
	[Custom06] [varchar](50) NULL,
	[Custom07] [varchar](50) NULL,
	[Custom08] [varchar](50) NULL,
	[Custom09] [varchar](50) NULL,
	[Custom10] [varchar](50) NULL,
	[isProrated] [bit] NOT NULL,
	[TagIdList] [text] NULL,
	[EnrollmentMaximum] [smallint] NULL,
	[TriggerMembershipTemplateId] [uniqueidentifier] NULL,
	[isRenewDownPayment] [bit] NULL,
	[isShowOnStudio] [bit] NULL,
	[OldMembershipTemplateId] [uniqueidentifier]
	)

Declare @Tmp_MembershipTemplateItem table (
											[MembershipTemplateItemId] [uniqueidentifier] NOT NULL,
											[PartitionId] [uniqueidentifier] NOT NULL,
											[MembershipTemplateId] [uniqueidentifier] NULL,
											[ProgramId] [uniqueidentifier] NOT NULL,
											[AppointmentGroupId] [uniqueidentifier] NULL)

/* Create Program in subscriber school */
insert into @Tmp_Program 
	(ProgramId, PartitionId,Name,Description,CreateDate,Custom01,Custom02,Custom03,Custom04,Custom05,IncomeCategoryId,isRanked,SystemValue,isProspectForm,OldProgramID)
Select 
	@TampaProgramID as ProgramID,@TampaPartitionID as PartitionID,Name,Description,CreateDate,Custom01,Custom02,Custom03,Custom04,Custom05,IncomeCategoryId,isRanked,SystemValue,isProspectForm,@TampaProgramID
from Program p
where p.ProgramId = @TemplateProgramID and p.PartitionId = @TemplatePartitionID 	

/* Create Membership Template Items For Subscriber Schools */
insert into @Tmp_MembershipTemplate 
	([MembershipTemplateId],[PartitionId],[Name],[Description],[BeginType],[BeginDate],[DurationCount],[DurationUnits],[AttendanceMaximumDuration],
	 [AttendanceMaximum],[isAutoRenew],[PaymentFrequency],[PaymentCount],[PaymentAmount],[isTaxable],[IncomeCategoryId],[Sort],[NotifyStaffId],[isShowOnPortal],
	 [Category],[PaymentType],[DueAtSignup],[EnrollmentBeginDate],[EnrollmentEndDate],[FirstPaymentAmount],[Custom01],[Custom02],[Custom03],[Custom04],[isTaxable2],
	 [AutoRenewId],[Custom05],[Custom06],[Custom07],[Custom08],[Custom09],[Custom10],[isProrated],[TagIdList],[EnrollmentMaximum],[TriggerMembershipTemplateId],
	 [isRenewDownPayment],[isShowOnStudio],[OldMembershipTemplateId])
select NEWID() as MembershipTemplateID, @TampaPartitionID as PartitionID,
       [Name],[Description],[BeginType],[BeginDate],[DurationCount],[DurationUnits],[AttendanceMaximumDuration],
	 [AttendanceMaximum],[isAutoRenew],[PaymentFrequency],[PaymentCount],[PaymentAmount],[isTaxable],[IncomeCategoryId],[Sort],[NotifyStaffId],[isShowOnPortal],
	 [Category],[PaymentType],[DueAtSignup],[EnrollmentBeginDate],[EnrollmentEndDate],[FirstPaymentAmount],[Custom01],[Custom02],[Custom03],[Custom04],[isTaxable2],
	 [AutoRenewId],[Custom05],[Custom06],[Custom07],[Custom08],[Custom09],[Custom10],[isProrated],[TagIdList],[EnrollmentMaximum],[TriggerMembershipTemplateId],
	 [isRenewDownPayment],[isShowOnStudio],
	 MembershipTemplateID as [OldMembershipTemplateId]
from MembershipTemplate 	
where PartitionId = @TemplatePartitionID 
	
insert into @Tmp_MembershipTemplateItem
			([MembershipTemplateItemId],[PartitionId],[MembershipTemplateId],
			 [ProgramId],[AppointmentGroupId])
			 
select newID() as membershipTemplateitemID,
	   @TampaPartitionID as PartitionID,   
	   tmp_mt.MembershipTemplateId,
	   @TampaProgramID as ProgramID,
	   null as AppointmentGroupID 
from MembershipTemplateItem mti
		inner join @Tmp_MembershipTemplate tmp_mt on mti.MembershipTemplateId = tmp_mt.OldMembershipTemplateId  
		
/* =================================================================================================== */

select * from @Tmp_Program 
select * from @Tmp_MembershipTemplate 	
select * from @Tmp_MembershipTemplateItem


if @RunThisScript = '1'
Begin
	/* ======= */
	insert into program
		(ProgramId, PartitionId,Name,Description,CreateDate,Custom01,Custom02,Custom03,Custom04,Custom05,IncomeCategoryId,isRanked,SystemValue,isProspectForm)
	select 	ProgramId,PartitionId,Name,Description,CreateDate,Custom01,Custom02,Custom03,Custom04,Custom05,IncomeCategoryId,isRanked,SystemValue,isProspectForm
	from @tmp_Program

	/* ============ */

	insert into MembershipTemplate 
	([MembershipTemplateId],[PartitionId],[Name],[Description],[BeginType],[BeginDate],[DurationCount],[DurationUnits],[AttendanceMaximumDuration],
		 [AttendanceMaximum],[isAutoRenew],[PaymentFrequency],[PaymentCount],[PaymentAmount],[isTaxable],[IncomeCategoryId],[Sort],[NotifyStaffId],[isShowOnPortal],
		 [Category],[PaymentType],[DueAtSignup],[EnrollmentBeginDate],[EnrollmentEndDate],[FirstPaymentAmount],[Custom01],[Custom02],[Custom03],[Custom04],[isTaxable2],
		 [AutoRenewId],[Custom05],[Custom06],[Custom07],[Custom08],[Custom09],[Custom10],[isProrated],[TagIdList],[EnrollmentMaximum],[TriggerMembershipTemplateId],
		 [isRenewDownPayment],[isShowOnStudio])
	Select
		[MembershipTemplateId],[PartitionId],[Name],[Description],[BeginType],[BeginDate],[DurationCount],[DurationUnits],[AttendanceMaximumDuration],
		 [AttendanceMaximum],[isAutoRenew],[PaymentFrequency],[PaymentCount],[PaymentAmount],[isTaxable],[IncomeCategoryId],[Sort],[NotifyStaffId],[isShowOnPortal],
		 [Category],[PaymentType],[DueAtSignup],[EnrollmentBeginDate],[EnrollmentEndDate],[FirstPaymentAmount],[Custom01],[Custom02],[Custom03],[Custom04],[isTaxable2],
		 [AutoRenewId],[Custom05],[Custom06],[Custom07],[Custom08],[Custom09],[Custom10],[isProrated],[TagIdList],[EnrollmentMaximum],[TriggerMembershipTemplateId],
		 [isRenewDownPayment],[isShowOnStudio]
	from @Tmp_MembershipTemplate 	 
	 
	 /* ===================== */
	 
	insert into MembershipTemplateItem
		([MembershipTemplateItemId],[PartitionId],[MembershipTemplateId],
				 [ProgramId],[AppointmentGroupId])
	select
		[MembershipTemplateItemId],[PartitionId],[MembershipTemplateId],
				 [ProgramId],[AppointmentGroupId]
	from @Tmp_MembershipTemplateItem 			 			 
End