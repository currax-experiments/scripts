/*
	Find All Active Messages for Credit Card Processor
	
	Hack and slash to pull the messagequeueid for a subset of the messages. 
	This identfies schools using blue for payment processing 

*/


select messagequeueid, begindate 
from MessageQueue
where messageQueueId in 
						(
						select t1.Messagequeueid 
						from (
								select messageQueueId, data, begindate, type, cast(substring(data,17,36) as uniqueidentifier) as partitionid  
								from MessageQueue 
								where type = 'daily-autopays' and retryCount > 0
							  ) t1

							inner join (
										select distinct PartitionId 
										from objectproperty op 
										where op.PropertyName in ('EFT_Component','CC_Component')
										and CAST(PropertyValue as varchar(100)) like '%blue%'	
										) t2  on t2.PartitionId = t1.partitionid
						and t1.partitionid = '199D9BE8-6108-4A09-ACA6-B76F37A9CAB9'
						)
						