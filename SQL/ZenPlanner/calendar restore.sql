use [ZenPlanner-Production] 
Go
/*
declare @PartitionID uniqueidentifier = '0AEF91B4-E9F8-40F2-AAB2-DDD0981364F7'

select a.AttendanceID, aP.AttendanceId, a.AppointmentID, aP.AppointmentID, a.appointmentGroupID, aP.AppointmentGroupId 
from Attendance a
		inner join [zenplanner-production].dbo.Attendance aP on (a.AttendanceId = aP.AttendanceID and a.PartitionId = @PartitionID and aP.PartitionId = @PartitionID) 
where a.PartitionId = @PartitionID 
go
*/
/*
Update Attendance
set AppointmentGroupID = null
where PartitionID = '0AEF91B4-E9F8-40F2-AAB2-DDD0981364F7'
*/

/*
update Attendance 
set AppointmentID = (select AppointmentId 
						  from [zenplanner-restore].dbo.Attendance 
						  where [zenplanner-restore].dbo.Attendance.attendanceID = [zenplanner-production].dbo.Attendance.AttendanceId 
						        and [zenplanner-production].dbo.Attendance.partitionID = '0AEF91B4-E9F8-40F2-AAB2-DDD0981364F7')					  
where Attendance.PartitionID = '0AEF91B4-E9F8-40F2-AAB2-DDD0981364F7'
	  and  attendance.AppointmentId IS NULL 
	  

update Attendance 
set AppointmentGroupID = (select AppointmentGroupId 
						  from [zenplanner-restore].dbo.Attendance 
						  where [zenplanner-restore].dbo.Attendance.attendanceID = [zenplanner-production].dbo.Attendance.AttendanceId 
						        and [zenplanner-production].dbo.Attendance.partitionID = '0AEF91B4-E9F8-40F2-AAB2-DDD0981364F7')					  
where Attendance.PartitionID = '0AEF91B4-E9F8-40F2-AAB2-DDD0981364F7'
	  and  attendance.AppointmentGroupId IS NULL 	  
	  

select AttendanceId, AppointmentId, AppointmentGroupId 
from Attendance 
where PartitionId = '0AEF91B4-E9F8-40F2-AAB2-DDD0981364F7' 
*/


