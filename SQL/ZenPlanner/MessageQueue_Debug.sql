-- total Messages in queue
select COUNT(*) as [  totalmessages in queue]
from MessageQueue mq
where mq.priority >= 0 and mq.priority <= 30
	 and mq.error IS NULL
	 and lockId IS NULL
	 and retryCount > 0
	 and beginDate < GETDATE()

-- Total messages by type
select COUNT(*) TotalMessages, type + ' '+  CONVERT(varchar(20), priority)
from MessageQueue mq
where mq.priority >= 0 and mq.priority <= 30
	 and mq.error IS NULL
	 and lockId IS NULL
	 and retryCount > 0
	 and beginDate < GETDATE()
group by  type + ' '+  convert(varchar(20),priority ), priority 
Order By priority asc

-- Messages added by hour/min
select COUNT(*) as NewMessages, datepart(hh,begindate) as hour, datepart(mi,begindate ) as minute
from MessageQueue mq
where mq.priority >= 0 and mq.priority <= 30
	 and mq.error IS NULL
	 and lockId IS NULL
	 and retryCount > 0
	 and beginDate < GETDATE()
Group By datepart(hh,begindate), datepart(mi,beginDate)
order by datepart(hh,begindate) Desc , datepart(mi,beginDate) desc

-- Oldest Messages in queue
Declare @StartPriority int = '1'
Declare @EndPriority int = '30'
Declare @ToleranceInMinutes int = '10'
Declare @CheckType varchar(15) = 'LessThan30'
Declare @CheckResult datetime 

set @CheckResult = (select min(beginDate) as EarliestBeginDate from MessageQueue mq
					where mq.priority >= @StartPriority and mq.priority <= @EndPriority  
							and mq.error IS NULL
							and lockId IS NULL
							and beginDate < GETDATE())

Select DateDiff(MINUTE,@CheckResult,GETDATE()) as OldestMessageInMinutes

select *
from MessageQueue 
where lockId is not null and error is null and beginDate < GETDATE()
order by beginDate desc
/*

*/