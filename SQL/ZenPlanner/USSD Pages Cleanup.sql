/*

.reason this was to remove the link for the custom website pages to shaolin kempo karate program where the pages were in both programs.

*/
/*
-- get the schools partitionID
select partitionID, schoolname from partition where schoolname like 'ussd%rancho%'

-- Get the programID's for Shaolin Kempo Karate and the Video Library
select distinct pe.pageid, pp.programid, pm.name
from page pe
		inner join pageprogram pp on pe.PageId = pp.PageId 
		inner join program pm on pm.programid = pp.programid
where pe.partitionID = 'E2655A2A-B417-44DC-B1AC-6C4145BC8374'

*/

declare @table table 
					(PageProgramid uniqueidentifier,
					 programID uniqueidentifier
					 )

insert into @Table
select pm.pageprogramid, pm.programID 
from pageprogram pm
		inner join 
						(
						select pe2.pageid, count(*) as quantity
						from page pe2
								inner join pageprogram pp on pe2.PageId = pp.PageId 
						where pe2.partitionID = 'E2655A2A-B417-44DC-B1AC-6C4145BC8374' --PartitionID of school we are cleaning up
							  and pp.programid in ('1A0AC20A-8BC2-42CB-BDDE-6DF9B0AD4756', '4F18274B-E424-4EA8-A671-40E947262BD3') --Shaolin Kempo Karate and the Video Library programID's
						group by pe2.pageid 
						having count(*) > 1
						) m 

						on m.pageid = pm.pageid 
where pm.programID = '4F18274B-E424-4EA8-A671-40E947262BD3' --Just the shaolin kempo karate programdi

-- delete from 
select * from pageprogram where pageprogramID in (select pageprogramID from @table)


         