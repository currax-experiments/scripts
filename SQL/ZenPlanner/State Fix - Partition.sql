/*
StateCodeCleanUp is a table manually created to address adhoc issues

*/

Partition State Clean Up
update partition 
set partition.state = rtrim(substring(s.stateCode,charindex('-',s.statecode,1)+1,len(s.statecode)))
-- select p.state, rtrim(substring(s.stateCode,charindex('-',s.statecode,1)+1,len(s.statecode))), s.StateName 
from partition p 
		inner join state s on s.StateName = p.state and rtrim(substring(p.locale,charindex('-',p.locale,1)+1,len(p.locale)))  = s.CountryCode 
select state from partition 

-- =========================================================================
update partition
set partition.state = s.StateCode 
-- select p.state, s.statecode
from partition p 
		inner join StateCleanUp s on s.StateToBeFixed = p.state and rtrim(substring(p.locale,charindex('-',p.locale,1)+1,len(p.locale)))  = s.Country 
Where StateCode Is Not NULL


/* Credit Card Table Clean UP */
update creditcard set creditcard.state = rtrim(substring(s.stateCode,charindex('-',s.statecode,1)+1,len(s.statecode)))
--select c.state, rtrim(substring(s.stateCode,charindex('-',s.statecode,1)+1,len(s.statecode))), s.StateName 
from creditcard c 
		inner join state s on s.StateName = c.state

-- =========================================================================

update creditcard set creditcard.state = s.StateCode 
-- select c.state, s.statecode
from creditcard c
		inner join StateCleanUp s on s.StateToBeFixed = c.state
Where s.StateCode Is Not NULL




-- validation
select count(*) as occurances, state 
from partition 
where state not in (select rtrim(substring(s.stateCode,charindex('-',s.statecode,1)+1,len(s.statecode))) as state from state s)
group by state

select count(*) as occurances, state 
from creditcard 
where state not in (select rtrim(substring(s.stateCode,charindex('-',s.statecode,1)+1,len(s.statecode))) as state from state s)
group by state 