select  
		pn.personID,
		Company,
		Allergies,
		AttendanceLast30Days,
		AttendanceSinceLastPromotionalTest,
		AttendanceSinceSignupDate
		BarCodeId,
		BirthDate,
		pn.CreateDate,
		Custom01,
		Custom02,
		Custom03,
		Custom04,
		Custom05,
		Custom06,
		Custom07,
		Custom08,
		Custom09,
		Custom10,
		Custom11,
		Custom12,
		Custom13,
		Custom14,
		Custom15,
		Custom16,
		Custom17,
		Custom18,
		Custom19,
		Custom20,
		Custom21,
		Custom22,
		Custom23,
		Custom24,
		Custom25,
		FirstAttendanceDate,
		pn.FirstName,
		Gender,
		LastAttendanceDate,
		LastLogDate,
		pn.LastName,
		Medications,
		MiddleName,
		Prefix,
		-- PrimaryAddress,
		a.Address1,
		a.Address2,
		a.City,
		a.[state],
		a.country,
		a.zipcode,
		PrimaryEmail,
		PrimaryPhoneNumber,
		ProspectPriorityId,
		-- ProspectStatusId,
		lc_prosectStatus.label as prospectStatus,
		-- ProspectStatusSubId,
		lc_prosectSubStatus.label as prospectSubStatus,
		pn.SignupDate,
		-- SourceId,
		-- SourceSubId,
		lc_source.label as [Source],
		lc_SourceSub.label as [SubSource],
		SpecialNeeds,
		pn.[status],
		pn.TrialEndDate,
		ViewDate,
		p.template as Vertical

from person pn
		left join [partition] p on pn.personID = p.parentPersonID 
		left join lookupcode lc_prosectStatus on lc_prosectStatus.lookupcodeID = pn.ProspectStatusId
		left join lookupcode lc_prosectSubStatus on lc_prosectSubStatus.lookupcodeID = pn.ProspectStatusSubId
		left join lookupcode lc_source on lc_source.lookupcodeID = pn.sourceID
		left join lookupcode lc_sourceSub on lc_sourceSub.lookupcodeID = pn.SourceSubId
		left join ContactInformation ci on ci.personid = pn.personID and ci.[type] = 'Address'
		left join [address] a on a.objectID = ci.ContactInformationID 

		
where pn.partitionID = 'B5E3A299-A135-E1F7-4974-B29ACA97B642' and pn.trialenddate >= '2006-01-01'
order by pn.company 
