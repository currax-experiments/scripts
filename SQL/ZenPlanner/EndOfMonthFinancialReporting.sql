declare @StartDate datetime = '2014-01-01 00:00:00',
		@EndDate Datetime = '2014-01-31 23:59:59',
		@ZPRoot uniqueidentifier = 'B5E3A299-A135-E1F7-4974-B29ACA97B642'

/* What we billed 
select p.company, b.DueAmount, b.DueDate, b.PaymentDate
from bill b
		inner join person p on b.personID = p.personID
where b.partitionID = 'B5E3A299-A135-E1F7-4974-B29ACA97B642'
		and b.duedate >= @StartDate and b.duedate <= @EndDate
*/

/* Payments Recieved 
select pn.company, pt.status, pt.amount, pt.PaymentDate, pt.PaymentGatewayId 
from payment pt
		inner join person pn on pt.personID = pn.personID
where pt.partitionID = 'B5E3A299-A135-E1F7-4974-B29ACA97B642'
		and pt.PaymentDate >= @StartDate and pt.PaymentDate <= @EndDate
*/
GO

with paymentCTE (PartitionID, Schoolname, Amount, Type)
AS
(select pn.partitionID,pn.Schoolname, pt.amount, cc.type 
	 from Payment pt
		 inner join partition pn on pt.partitionID = pn.partitionID 
		 left join creditcard cc on cc.CreditCardID = pt.creditCardID
	  where pt.Status = 'approved'	
	 	 and pt.PartitionId != 'B5E3A299-A135-E1F7-4974-B29ACA97B642'
		 and pn.Status in ('member','Expired','Canceled')
		 and pt.PaymentDate >= '2014-02-01 00:00:00' and pt.PaymentDate <= '2014-02-28 23:59:59'
--Group by pn.schoolname       
)
select SchoolName,  [Type], cast(op_cc.PropertyValue as varchar(100)) as processor, sum(amount) as totalAmount
from PaymentCTE pcte
		left join ObjectProperty op_cc on (pcte.PartitionId = op_cc.PartitionId and op_cc.PropertyName = 'CC_Component')
		
where Type = 'Credit'
group by SchoolName, [Type], cast(op_cc.PropertyValue as varchar(100))

Union

select SchoolName, [Type], cast(op_eft.PropertyValue as varchar(100)) as processor, sum(amount) as totalAmount
from PaymentCTE pcte
		left join ObjectProperty op_eft on (pcte.PartitionId = op_eft.PartitionId and op_eft.PropertyName = 'eft_Component')
where Type = 'EFT'
group by SchoolName, [Type], cast(op_eft.PropertyValue as varchar(100))

Union 

select SchoolName, [Type], ' ' as Processor, sum(amount) as totalAmount
from PaymentCTE pcte
where Type is Null 
group by SchoolName,  [Type]
order by SchoolName

GO

/* Use this to more easily identify the payment processor name
	   CreditCardPaymentProcessor = 
		   case convert(nvarchar(100),op_cc.propertyValue)
				When 'ACHDirect_CC' then 'ACHDirect'
				when 'AuthorizeNet_CC' then 'AuthorizeNet'
				when 'Beanstream' then 'Beanstream'
				when 'Beanstream_CC' then 'Beanstream'
				when 'Bluefin' then 'Bluefin'
				when 'ExactPay' then 'ExactPay'
				when 'EziDebit' then 'EZiDebit'
				when 'FirstData' then 'FirstData'
				when 'OptimalPayments' then 'OptimalPayments'
				when 'PayMeNow' then 'PayMeNow'
				when 'PaymentExpress_CC' then 'PaymentExpress'
				when 'PaymentHub' then 'PaymentHumb'
				when 'PaySimple_CC' then 'PaySimple'
				when 'PaySimple30'  then 'PaySimple'
				when 'PostFinance_CC' then 'PostFinance'
				when 'Sidekick_CC' then 'Sidekick'
				else ' '
			end	,
	   EFTPaymentProcessor =
		case convert(nvarchar(100),op_eft.propertyValue)
			when 'ACHDirect_EFT' then 'ACHDirect'
			when 'AuthorizeNet_EFT' then 'AuthorizeNet'
			when 'Beanstream' then 'Beanstream'
			when 'Beanstream_EFT' then 'Beanstream'
			when 'Bluefin' then 'Bluefin'
			when 'EziDebit' then 'EZiDebit'
			when 'GulfManagement_EFT' then 'GulfManagement'
			when 'GulfManagement_EFT_Disabled' then 'GulfManagement'
			when 'PayMeNow' then 'PayMeNow'
			when 'PaymentExpress_CC' then 'PaymentExpress'
			when 'PaymentHub' then 'PaymentHub'
			when 'PaySimple_EFT' Then 'PaySimple'
			when 'PaySimple30' then 'PaySimple'
			when 'Sidekick_EFT' then 'SideKick'
			when 'Test_APPROVED' then 'Test Approved'
			when 'VersaPay' then 'VersaPay'
			else ' '
		
		end,

*/