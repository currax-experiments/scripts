SELECT tst.session_id 
FROM sys.dm_tran_session_transactions tst 
		INNER JOIN sys.dm_exec_connections ec ON tst.session_id = ec.session_id
		inner join sys.dm_exec_sessions ses on ses.session_id = ec.session_id 
		CROSS APPLY sys.dm_exec_sql_text (ec.most_recent_sql_handle) AS sqltext
where tst.is_user_transaction = '1' 
		and tst.session_id not in (select req.session_id
									FROM sys.dm_exec_requests req
										inner join sys.dm_exec_sessions ses on ses.session_id = req.session_id 
									CROSS APPLY sys.dm_exec_sql_text(sql_handle) AS sqltext
									where text not like 'select sqltext%'
									)
if (@@ROWCOUNT  >= 0)
begin
	insert into [ZenPlanner-Management].dbo.[OrphanedTransactionsLog]
		(sessionID, server, query)
	SELECT tst.session_id as sessionID, ses.host_name as server, sqltext.text as query
	FROM sys.dm_tran_session_transactions tst 
			INNER JOIN sys.dm_exec_connections ec ON tst.session_id = ec.session_id 
			inner join sys.dm_exec_sessions ses on ses.session_id = ec.session_id 
			CROSS APPLY sys.dm_exec_sql_text (ec.most_recent_sql_handle) AS sqltext
	where tst.is_user_transaction = '1' 
			and tst.session_id not in (select req.session_id
										FROM sys.dm_exec_requests req
											inner join sys.dm_exec_sessions ses on ses.session_id = req.session_id 
										CROSS APPLY sys.dm_exec_sql_text(sql_handle) AS sqltext
										where text not like 'select sqltext%'
										) 

end