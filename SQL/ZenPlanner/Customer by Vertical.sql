declare @StartDate smalldatetime = '2014-02-01 00:00:00',
        @EndDate   smalldatetime = '2014-02-28 23:59'


/* New Customers */
select p.schoolname, p.Template as Vertical, p.status, pn.firstname, pn.lastname, cast(pn.signupdate as date) as SignUpDate
from person pn
		inner join partition p on pn.personID = p.parentPersonID 
where pn.signupdate >= @StartDate and pn.signupdate <= @EndDate
and pn.partitionID =  'B5E3A299-A135-E1F7-4974-B29ACA97B642'


/* Free Trials Expired */
select p.schoolname, p.Template as Vertical, p.status, pn.firstname, pn.lastname, cast(p.createdate as date) as inquiredate
from person  pn
		inner join partition p on pn.personID = p.parentPersonID 
where pn.createdate >= @StartDate and pn.createdate <= @EndDate
 and pn.partitionID =  'B5E3A299-A135-E1F7-4974-B29ACA97B642'
 and p.schoolname not like '%test%'


