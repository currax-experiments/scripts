use [zenplanner-production]
/*


*/


/*
select partitionid, schoolname
from partition 
where schoolname like 'USSD Test 3a'

Schools to fix
-- ############################################
'1C00398D-B413-40C5-89E2-7486D17690AF' - Mission Viejo North
'AAF6040F-5D48-4045-9B0D-1BA6A77E7B47' - Foothill Ranch (Fine)
'72E9860F-0556-40AE-AF2E-EE78FA363E79' - Dana Point (Fine)
'E2655A2A-B417-44DC-B1AC-6C4145BC8374' - Rancho Santa Margarita

Test Schools
-- #############################################
'4C0545A3-8C29-41E5-B7A8-53B314D959F4' - Garner School of Dance
'D238F6A7-4A22-478C-BCD7-47D6CDE9598A' - USSD - Anna Test 456
'4227B85B-E62D-4749-B2AA-3E4FDACCA29C' - USSD Test 2
'4470DB8F-6743-4D61-A051-B2169DBBA40D' - USSD Test 3
'75DE5B13-1E3D-41E8-AB48-8D7E369AFA49' - USSD Test 3a

 

-- Clean Up Script
https://preview.zenplanner.com/zenplanner/studio/tools/ussdsharingpatch/ussd-cleanup.cfm?PartitionIDToFix= 


/*
-- Increase the priority of the shared object push "stuff"
update messagequeue set priority = '1' where type like '%sharedobject%' and retrycount > 0
select * from messagequeue where type like '%sharedobject%' and retrycount > 0
select * from messagequeue where type like '%sharedobject%' and retrycount = 0
*/

*/

Declare @USSDCorpPart uniqueidentifier = '83D64375-8E0B-41AB-88C9-E871251CFD5A'
Declare @WebSiteShareGroupID uniqueidentifier = '3BB86EFD-9E6C-43ED-8F5E-1325D5350CFC'
Declare @CorpShareGroupID uniqueidentifier = '5D2F01C3-9045-4271-B30A-A25795FFFC77'
Declare @TestPartition uniqueidentifier = '75DE5B13-1E3D-41E8-AB48-8D7E369AFA49'

Declare @SharedShadowObjects table
								(
								sharedObjectID uniqueidentifier,
								shadowobjectID uniqueidentifier,
								objectid	   uniqueidentifier,
								class			varchar(200),
								subscriberPartitionID uniqueidentifier,
								publisherPartitionID  uniqueidentifier
								)

insert into @SharedShadowObjects
select so.sharedObjectId, 
		shdw.shadowObjectId,
		shdw.objectid, 
		shdw.class, 
		shdw.partitionID as subscriberPartitionID,
		so.partitionID as publisherPartitionID
from SharedObject so
		inner join shadowobject shdw on shdw.sharedObjectId = so.sharedObjectId 
		-- inner join partition p on p.partitionID = shdw.partitionID
where so.ShareGroupID = @WebSiteShareGroupID 
		and so.partitionID = @USSDCorpPart
		and shdw.partitionID in ('AAF6040F-5D48-4045-9B0D-1BA6A77E7B47','72E9860F-0556-40AE-AF2E-EE78FA363E79','1C00398D-B413-40C5-89E2-7486D17690AF','E2655A2A-B417-44DC-B1AC-6C4145BC8374')
		-- and shdw.partitionID = @TestPartition
order by so.class 	

-- ## Attachments
--select * 
delete
from Attachment  
where attachmentID in (select objectID from @SharedShadowObjects where class = 'Attachment')

-- ## Automation
-- select * 
delete
from Automation  
where AutomationID in (select objectID from @SharedShadowObjects where class = 'Automation')

-- ## Automation
-- select * 
delete
from LookupCode  
where LookupCodeID in (select objectID from @SharedShadowObjects where class = 'LookupCode')

-- ## MembershipTemplateItem
-- select * 
delete 
from MembershipTemplateItem  
where MembershipTemplateItemID in (select objectID from @SharedShadowObjects where class = 'MembershipTemplateItem')

-- ## MembershipTemplate
-- select * 
delete
from MembershipTemplate  
where MembershipTemplateID in (select objectID from @SharedShadowObjects where class = 'MembershipTemplate')

-- ## ObjectProperty
-- select * 
delete
from ObjectProperty  
where ObjectPropertyID in (select objectID from @SharedShadowObjects where class = 'ObjectProperty')

-- ## Page
-- select * 
delete
from Page  
where PageID in (select objectID from @SharedShadowObjects where class = 'Page')

-- ## PageProgram
-- select * 
delete
from PageProgram  
where PageProgramID in (select objectID from @SharedShadowObjects where class = 'PageProgram')

-- ## ShadowObjects
-- select * 
delete
from ShadowObject 
where partitionID in ('AAF6040F-5D48-4045-9B0D-1BA6A77E7B47','72E9860F-0556-40AE-AF2E-EE78FA363E79','1C00398D-B413-40C5-89E2-7486D17690AF','E2655A2A-B417-44DC-B1AC-6C4145BC8374')
	-- partitionID = @TestPartition
	and shareGroupId = @WebSiteShareGroupID

-- ## PartitionShareGroup
-- select * 
delete 
from PartitionShareGroup
where partitionID in ('AAF6040F-5D48-4045-9B0D-1BA6A77E7B47','72E9860F-0556-40AE-AF2E-EE78FA363E79','1C00398D-B413-40C5-89E2-7486D17690AF','E2655A2A-B417-44DC-B1AC-6C4145BC8374')
	  -- partitionID = @TestPartition
	  and shareGroupID = @WebSiteShareGroupID

