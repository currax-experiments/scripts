use [zenplanner-restore]
Declare @PartitionID uniqueidentifier = 'ED46D293-9E3E-4DDD-8350-E2695424D7EE'
/*
insert into [ZenPlanner-Production].dbo.AppointmentGroup
select *
from AppointmentGroup
where PartitionId = @PartitionID 
		and AppointmentGroupId not in (select AppointmentGroupId from [ZenPlanner-Production].dbo.AppointmentGroup where PartitionId = @PartitionID)
*/		

/*
insert into [ZenPlanner-Production].dbo.AppointmentGroupSchedule
Select *
from AppointmentGroupSchedule 
where PartitionId = @PartitionID 
		and AppointmentGroupScheduleId not in (select AppointmentGroupScheduleId from [ZenPlanner-Production].dbo.AppointmentGroupSchedule where PartitionId = @PartitionID)
*/

/*
-- insert into [ZenPlanner-Production].dbo.Appointment
select * 
from Appointment 
where PartitionId = @PartitionID 
		and AppointmentId not in (select AppointmentId from [ZenPlanner-Production].dbo.Appointment where PartitionId = @PartitionID)
*/