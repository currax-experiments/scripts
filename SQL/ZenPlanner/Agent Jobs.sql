-- auto pay patch
insert into MessageQueue
(
	messageQueueId,
	type,
	priority,
	retryCount,
	beginDate,
	data,
	signature
)
select
	newId() as messageQueueId,
	'daily-autopay-creditCard' AS type,
	1 AS priority,
	2 as retryCount,
	getDate() as beginDate,
	'{partitionId="' + cast(partitionId AS varchar(36)) + '", creditCardId="' + cast(creditCardId as varchar(36)) + '", schoolName=""}' AS data,
	creditCardId as signature

-- use this "from" block to find incomplete autopays
from CreditCard
where creditCardId IN
(
select creditCardId
from view_Autopay2
where dueDate BETWEEN  DateAdd(day, -5, getDate()) AND GETDATE()
)
