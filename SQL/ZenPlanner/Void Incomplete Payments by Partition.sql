/* Affect Schools */
/*
select schoolname, prtn.partitionID 
from partition prtn
		inner join payment pt on pt.partitionID = prtn.partitionID
where pt.status = 'Incomplete'
		and pt.paymentdate >= '2014-07-06 00:00:00'
*/

declare @incompletePayment Table
			(
				PartitionID				uniqueidentifier,
				PaymentID				uniqueidentifier,
				PaymentAllocationID		uniqueidentifier,
				PersonID				uniqueidentifier,
				paymentdate				smalldatetime,
				amount					money,
				BillID					uniqueidentifier,
				PaymentType				Varchar(100),
				status					varchar(100),
				creditcardID			uniqueidentifier 
			)

insert into @incompletePayment
select pt.PartitionID, PT.paymentID, pa.paymentallocationID, pt.personID, pt.paymentDate, pt.amount, pa.BillId, paymentType, pt.status, pt.creditcardID
from payment pt
		left join paymentallocation pa on pa.paymentiD = pt.paymentID 
		left join bill b on b.billid = pa.billid
where pt.partitionid in ('0AFB0B24-808C-4520-A630-BACC2C27EB73',
'E28527A8-6EE7-4A24-F2BD-BF8B8D1F47F6',
'9BBB62B6-E66A-467A-E7B4-C89762BFB6FC',
'A90EA569-64CA-4926-BC85-CE2AF2108E01',
'666150B0-C266-4652-9698-D2B4C7D877B6',
'3871AA63-3D4A-4D9C-C572-D4CCF6DF9413',
'F932FE6B-7AA1-4AC4-B56A-D6B66E5294F5',
'EBF16477-3E2E-4E93-D33A-D6D724C1A2FF',
'C3FBCBC2-C9BF-49E0-B0CD-D8A01244A541',
'FFD61667-522B-44D1-E14F-DFB3A860780B',
'31D252AD-2A9A-47FD-EF2C-E85B2CA381A3',
'60FE2A20-6CEF-4ED2-B108-E89AE42580E3',
'61D260CA-762C-4757-96EB-ECDE3597608B',
'75C65044-5007-4B3B-EDB5-EF260879D125',
'7002A530-60E9-4568-B0D2-EFC5C9FCD205',
'C4F19102-AC31-43CA-F4DB-EFE19DA08531',
'EA54AE70-2372-4AE2-C3A3-F1C09F77B0B9',
'C19737D7-6903-4CC4-B088-F226705976E0',
'E050D7BE-27F9-4B24-98F6-F2BB5B659FA1',
'2AE23D4C-64E1-4D1D-A7AB-F2F6636069B8',
'BB711399-D7ED-4129-949C-F492DF3549DC',
'2416A189-7363-4073-9E9C-F5F90F179A1D',
'7BC57401-3BCC-4042-A569-F6858D97E515',
'4060598E-38C6-42B4-A36A-F6B348A09D4C',
'94757BE6-3B42-4407-B1F0-F7AF5260B7F3',
'5476D282-DC65-47AB-8C61-F8009D86F23C',
'07320738-8255-408A-C2F8-F842888CFEC4',
'E47964A6-6142-4CF9-8244-FB844E954ACD'
							)
		
		and pt.status = 'Incomplete'
		and pt.paymentdate >= '2014-07-06 00:00:00'

select * 
from @incompletePayment

/* Set the Bill Payment Date to be null ------------------------------------------------------------------------------------------------- */
insert into [zenplanner-management].dbo.bill_backup
select *
from bill 
where billID in (select Billid from @incompletePayment)
		and billID not in (select Billid from [zenplanner-management].dbo.bill_backup)

update bill set paymentdate = Null
--select billid, paymentdate from bill
where billID in (select Billid from @incompletePayment)


/* Delete the PaymentAllocation for the bill  ------------------------------------------------------------------------------------------- */
insert into [zenplanner-management].dbo.PaymentAllocation_backup
select *
from PaymentAllocation
where PaymentAllocationID in (select PaymentAllocationID from @incompletePayment)
		and PaymentAllocationID not in (select PaymentAllocationID from [zenplanner-management].dbo.PaymentAllocation_backup)

delete
--select paymentAllocationID
from paymentallocation 
where paymentAllocationID in (select PaymentAllocationID from @incompletePayment)

/* update the payment status to be void ------------------------------------------------------------------------------------------------------ */
insert into [zenplanner-management].dbo.Payment_backup
select *
from Payment
where PaymentID in (select PaymentID from @incompletePayment)
		and PaymentID not in (select PaymentID from [zenplanner-management].dbo.Payment_backup)

update payment set Status = 'VOID'
-- select paymentID from payment 
where PaymentID in (select paymentid from @incompletePayment)


/* Clear the Error Flag on the Credit Card ------------------------------------------------------------------------------------------------------ */
insert into [zenplanner-management].dbo.creditcard_backup
select *
from creditcard
where creditcardID in (select creditcardID from @incompletePayment)
		and creditcardID not in (select creditcardID from [zenplanner-management].dbo.creditCard_backup)

update CreditCard set creditcard.isError = 0
-- select CreditCardID from creditcard
where CreditCardID in (select CreditCardID from @incompletePayment)

/*
-- To ReProcess Bills
insert into MessageQueue
(
	messageQueueId,
	type,
	priority,
	retryCount,
	beginDate,
	data,
	signature
)
select
	newId() as messageQueueId,
	'daily-autopay-creditCard' AS type,
	1 AS priority,
	2 as retryCount,
	getDate() as beginDate,
	'{partitionId="' + cast(partitionId AS varchar(36)) + '", creditCardId="' + cast(creditCardId as varchar(36)) + '", schoolName=""}' AS data,
	creditCardId as signature

-- use this "from" block to find incomplete autopays
from CreditCard
where creditCardId IN
(
select creditCardId
from view_Autopay2
where dueDate BETWEEN  DateAdd(day, -5, getDate()) AND GETDATE()
)
*/