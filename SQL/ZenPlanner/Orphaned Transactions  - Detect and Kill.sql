DECLARE @KillTxt VARCHAR(MAX) = ' ' 
DECLARE @OrphanedTransactions table
								(
									SessionID		int,
									[Server]		varchar(200),
									[Query]			varchar(Max),
									[Login_name]	varchar(200)
								)


SELECT tst.session_id 
FROM sys.dm_tran_session_transactions tst 
		INNER JOIN sys.dm_exec_connections ec ON tst.session_id = ec.session_id
		inner join sys.dm_exec_sessions ses on ses.session_id = ec.session_id 
		CROSS APPLY sys.dm_exec_sql_text (ec.most_recent_sql_handle) AS sqltext
where tst.is_user_transaction = '1' 
		and tst.session_id not in (select req.session_id
									FROM sys.dm_exec_requests req
										inner join sys.dm_exec_sessions ses on ses.session_id = req.session_id 
									CROSS APPLY sys.dm_exec_sql_text(sql_handle) AS sqltext
									where text not like 'select sqltext%'
									)
if (@@ROWCOUNT  > 0)
begin
	insert into @OrphanedTransactions
             (sessionID, Server, query,login_name)
	SELECT tst.session_id as sessionID, ses.host_name as server, sqltext.text as query, ses.login_name 
	FROM sys.dm_tran_session_transactions tst 
			INNER JOIN sys.dm_exec_connections ec ON tst.session_id = ec.session_id
			inner join sys.dm_exec_sessions ses on ses.session_id = ec.session_id 
			CROSS APPLY sys.dm_exec_sql_text (ec.most_recent_sql_handle) AS sqltext
	where tst.is_user_transaction = '1' 
			and tst.session_id not in (select req.session_id
										FROM sys.dm_exec_requests req
											inner join sys.dm_exec_sessions ses on ses.session_id = req.session_id 
										CROSS APPLY sys.dm_exec_sql_text(sql_handle) AS sqltext
										where text not like 'select sqltext%'
										) 

	SELECT  @KillTxt = @KillTxt + 'Kill ' +  cast(ses.session_id as varchar(10)) + N'; '
	FROM sys.dm_tran_session_transactions tst 
			INNER JOIN sys.dm_exec_connections ec ON tst.session_id = ec.session_id
			inner join sys.dm_exec_sessions ses on ses.session_id = ec.session_id 
			CROSS APPLY sys.dm_exec_sql_text (ec.most_recent_sql_handle) AS sqltext
	where tst.is_user_transaction = '1' 
			and tst.session_id not in (select req.session_id
										FROM sys.dm_exec_requests req
											inner join sys.dm_exec_sessions ses on ses.session_id = req.session_id 
										CROSS APPLY sys.dm_exec_sql_text(sql_handle) AS sqltext
										where text not like 'select sqltext%'
										)
			and exists (select * 
			             from [ZenPlanner-Management].dbo.[OrphanedTransactionsLog] otl 
						 where otl.sessionID = tst.session_ID 
								and otl.createdatetime >= DATEADD(SECOND,-120,getdate())
								and otl.createdatetime <= DATEADD(SECOND,-60,getdate())
						)

	 insert into [ZenPlanner-Management].dbo.[OrphanedTransactionsLog]
		(sessionID, Server, query,login_name)
	 select sessionID, [Server], query,login_name
	 from @OrphanedTransactions

	if len(rtrim(ltrim(@KillTxt))) > 0
	Begin
		-- Select @KillTxt 
		insert into [ZenPlanner-management].dbo.[KilledTransaction]
			([data])
		Values
			(@KillTxt)

		Exec (@KillTxt)
	End
end



