with mq_cte (type,partitionID, Data, begindate) as
(
select type, cast(substring(data,17,36) as uniqueidentifier) as partitionID, data, begindate 
from MessageQueue 
where type like '%daily-autopays%'
		and retryCount > 0
)

select data, begindate, prtn.schoolname 
from mq_cte a
		inner join 
					(
					select ObjectPropertyID, PartitionID,ObjectID, Class,PropertyName,PropertyValue
					from objectproperty op
					where op.propertyname like 'cc_component'
							and  cast(propertyvalue as varchar(100)) in ('PaySimple40', 'PaySimple30')

					) m on m.partitionID = a.partitionID 
		inner join partition prtn on prtn.partitionID = a.partitionID 
order by begindate desc

