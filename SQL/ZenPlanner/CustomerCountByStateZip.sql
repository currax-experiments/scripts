declare @PartitionID uniqueidentifier = 'B5E3A299-A135-E1F7-4974-B29ACA97B642' -- zen planner root

select State, zipcode, COUNT(*) as numberOfCustomers
from 
		(
		select Distinct p.Company, a.ZipCode, a.[State], a.Country
		from Person p
				inner join Membership m on m.PersonId = p.PersonId 
				inner join ContactInformation on p.PersonId = ContactInformation.personId 
				inner join [Address] a on ContactInformation.ContactInformationId = a.ObjectId and a.Class = 'contactinformation'
				
		where p.PartitionId = @PartitionID 
				and m.BeginDate >= '2013-01-01 00:00:00' and EndDate <= '2013-12-31 00:00:00'
				and a.Country = 'US'
	
		) m
group by m.State, m.ZipCode  