use [zenplanner-restore]
/* Enumeration for Partition.Status PAYMENT-PROBLEM, MEMBER, CANCELED, COMPLIMENTARY, FREE TRIAL
   Enumberation for Person.Status STUDENT, NULL, CLOSED, PROSPECT, ALUMNI
   First Day of last month: DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 1, 0)
   Last Day of Last month: DATEADD(DAY, -(DAY(GETDATE())), GETDATE())

*/
declare @FirstDayOfLastMonth datetime = convert(datetime, cast(cast(DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 1, 0) as date) as varchar(40)) + ' 00:00:00', 120)
declare @LastDayofLastMonth datetime = convert(datetime, cast(cast(DATEADD(DAY, -(DAY(GETDATE())), GETDATE()) as date) as varchar(40)) + ' 23:59:59', 120)

select  p.schoolName as [Database Name],
		p.Firstname + p.LastName as [Owner Name],
		p.BusinessPhone as [Phone Number],
		p.status as [Database Status],
		convert(varchar,p.CreateDate,110) as [Inquire Date],
		convert(varchar,p.signupDate, 110) as [Sign Up Date],
		(Select COUNT(*) from Person pn where pn.PartitionId = p.partitionID and Status is not NULL and Status in ('Student') ) as [Current Memebership],
		(Select COUNT(*) from Person pn where pn.PartitionId = p.partitionID and Status is not NULL and Status in ('Student') and SignupDate <= '12/31/2011 23:59:59.000') as [2011 Membership],
		(Select COUNT(*) from Person pn where pn.PartitionId = p.partitionID and Status is not NULL and Status in ('Student') and SignupDate <= '12/31/2012 23:59:59.000') as [2012 Membership],
		
	    p.Pricing   as [Pricing Schedule],
	    '$ ' + convert(varchar, cast((select SUM(Amount) from Payment pt where pt.PartitionId = p.partitionID and PaymentDate >= @FirstDayOfLastMonth and PaymentDate <= @LastDayofLastMonth) as money),1) as [Last Months Revenue], 
	    '$ ' + convert(varchar, cast((select SUM(Amount) from Payment pt where pt.PartitionId = p.partitionID and PaymentDate >= convert(datetime,'12/1/2011 00:00:00.000', 120)
			and PaymentDate <= convert(datetime,'12/31/2011 23:59:59.000',120)) as money),1) as [December 2011 Revenue],
		'$ ' + convert(varchar, cast((select SUM(Amount) from Payment pt where pt.PartitionId = p.partitionID and PaymentDate >= convert(datetime, '2012-12-01 00:00:00.000', 120) 
			and PaymentDate <= convert(datetime, '2012-12-31 23:59:59.000', 120)) as money),1) as [December 2012 Revenue],
		'$ ' + convert(varchar, cast((select SUM(Amount) from Payment pt where pt.PartitionId = p.partitionID and PaymentDate >= convert(datetime, '2011-1-01 00:00:00.000', 120) 
			and PaymentDate <= convert(datetime, '2011-12-31 23:59:59.000', 120)) as money),1) as [Total 2011 Revenue],
		'$ ' + convert(varchar, cast((select SUM(Amount) from Payment pt where pt.PartitionId = p.partitionID and PaymentDate >= convert(datetime, '2012-1-01 00:00:00.000', 120) 
			and PaymentDate <= convert(datetime, '2012-12-31 23:59:59.000', 120)) as money),1) as [Total 2012 Revenue]    
from Partition p
where p.Status not in ('EXPIRED','CANCELED')
Order by SchoolName Asc
