-- =========================================================================
declare @CurrentTable varchar(200) = '', 
		@PartitionID uniqueidentifier = 'F9865470-C45C-41B1-9A65-17D7E0F52F59', 
		@SQLCMD_Del varchar(max) = '',
		@SQLCMD_ins varchar(max) = '',
		@SourceDatabase varchar(50) = '',
		@TargetDatabase varchar(50) = '',
		@List varchar(5000) = null,
		@RealRun Bit = 1	
-- =========================================================================

-- Disable Foreign Keys in Target Database
use [zenplanner-Development]
EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

use [wot_data] 
IF CURSOR_STATUS ('global','TableList') >= -1
BEGIN
	DEALLOCATE TableList
END

-- Copy Table data specific to Partition
DECLARE TableList CURSOR FOR
	SELECT TABLE_NAME 
	FROM  [ZenPlanner-development].[INFORMATION_SCHEMA].tables 
	where TABLE_TYPE = 'Base Table' 
		and Table_Name not in ('session','_AccessLog','LoginAttempt',
							   'LogAutomation','FirewallViolation','Country',
							   'dtproperties','Help','Email','MessageQueue',
							   'MessageQueueHistory','Log','LoginAttempts',
							   'MessageQueueStats','PortletType',
							   'PricingAddOn','ScrapedProspect','State',
							   'SystemAnnouncement','Test','Theme','
							   UserAccountReset') 
	intersect 
	SELECT TABLE_NAME 
	FROM  [wot_data].[INFORMATION_SCHEMA].tables 
	where TABLE_TYPE = 'Base Table' 
		and Table_Name not in ('session','_AccessLog','LoginAttempt',
							   'LogAutomation','FirewallViolation','Country',
							   'dtproperties','Help','Email','MessageQueue',
							   'MessageQueueHistory','Log','LoginAttempts',
							   'MessageQueueStats','PortletType',
							   'PricingAddOn','ScrapedProspect','State',
							   'SystemAnnouncement','Test','Theme','
							   UserAccountReset') 	
	order by Table_name asc

OPEN TableList
FETCH NEXT FROM TableList into @CurrentTable
While @@FETCH_STATUS = 0
	BEGIN

		select @List = coalesce(@List + '],[','') + column_name
		from 
			(
			SELECT COLUMN_NAME 
			FROM [wot_data].INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = @CurrentTable
			intersect 
			SELECT COLUMN_NAME 
			FROM [ZenPlanner-development].INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = @CurrentTable
			) M

		set @List = '[' + @List + ']'

		
		-- Delete all data from Target table for the partition	
		set @SQLCMD_del = 'Delete from [zenplanner-development].dbo.[' + @CurrentTable + '] where PartitionID = ''' + cast(@PartitionID as varchar(45))+ ''''
		print @SQLCMD_del
		
		-- Insert all the data from the source table to the target table for the partition
		set @SQLCMD_ins = 'insert into [zenplanner-development].dbo.[' + @CurrentTable + '] 
						   ( ' + @List + ')
						   select ' + @List + ' from [' + @CurrentTable + '] where PartitionID = ''' + cast(@PartitionID as varchar(45))+ ''''
		print @SQLCMD_ins
	
		Print ' '
		
		if @RealRun = 1 
			Begin
			exec(@SQLCMD_Del)
			exec(@SQLCMD_ins)
			END 
		
		
		set @SQLCMD_del = ''
		set @SQLCMD_ins = ''
		set @List = NULL
		FETCH NEXT FROM TableList into @CurrentTable
	END
	
CLOSE TableList
DEALLOCATE TableList
-- Copy Table Data that is global in nature

if @RealRun = 1
Begin
	Truncate table [zenplanner-dev].dbo.[Country] 
	insert into [zenplanner-development].dbo.[Country]  select * from [country]

	delete from [zenplanner-development].dbo.[PortletType] 
	insert into [zenplanner-development].dbo.[PortletType]  select * from [PortletType]
	/*
	Truncate table [zenplanner-dev].dbo.[PricingAddOn] 
	insert into [zenplanner-dev].dbo.[PricingAddon]  select * from [PricingAddOn]
	*/

	Truncate table [zenplanner-dev].dbo.[State] 
	insert into [zenplanner-development].dbo.[State]  select * from [State]
END


