select p.schoolname, 
	   p.Template as Vertical, 
	   p.status, 
	   cast(p.createdate as date) as inquiredate, 
	   cast(pn.signupdate as date) as SignUpDate,
	   upper(right(p.locale,2)) as country,
	   (select count(*) from person where status = 'Student' and partitionID = p.partitionID) as StudentCount

	   

from person  pn
		inner join partition p on pn.personID = p.parentPersonID 
		-- inner join personprogram pp on pp.programID = 'D512C89F-FF22-06E6-F875-FF4FE315A741' and pn.personId = pp.personId 

where -- pn.createdate >= '2014-01-01 00:00:00' and pn.createdate <= '2014-01-31 23:59' 
		 pn.partitionID =  'B5E3A299-A135-E1F7-4974-B29ACA97B642'
		 and p.schoolname not like '%test%'
Order by p.schoolname 
		 

/*
select * from program where partitionId = 'B5E3A299-A135-E1F7-4974-B29ACA97B642'
select * from personprogram where programID = 'D512C89F-FF22-06E6-F875-FF4FE315A741'
*/