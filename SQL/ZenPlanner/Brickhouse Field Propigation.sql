declare @PartitionThatHasCustomFields uniqueidentifier = '681329F1-4848-4E45-B058-A1A466991CE3'
declare @BrickhouseParentPartitionID uniqueidentifier = '51D8F4ED-29F5-4F57-8841-2340D56C6FEF' --Brickhouse Cardio Club Corporate

/*
select *
from CustomField 
where PartitionId = '681329F1-4848-4E45-B058-A1A466991CE3'
		and Label like '%health%'
*/

/* delete the healthways field stuff 
select *
-- delete 
from CustomField
where Label like '%health%ways%'
		and PartitionId not in (@PartitionThatHasCustomFields )
		and PartitionId in (select partitionID
							  from Partition 
							  where (ParentPartitionId = @BrickhouseParentPartitionID or partitionID = @BrickhouseParentPartitionID)
									and PartitionId <> @PartitionThatHasCustomFields )
*/
									
/*	
insert into CustomField 
select NEWID() as CustomFieldID,
	   m.PartitionId as partitionID,
	   'Person' as Class,
	   'Custom24' as FieldName,
	   'Healthways' as Label,
	   'yesNo' as Type,
	   '3' as sort,
	   NULL as params,
	   '1' as IsImportant,
	   'Custom' as Location
From (select partitionID,schoolname 
	  from Partition 
	  where (ParentPartitionId = @BrickhouseParentPartitionID or partitionID = @BrickhouseParentPartitionID)
			and PartitionId <> @PartitionThatHasCustomFields ) m
*/
/*
insert into CustomField 
select NEWID() as CustomFieldID,
	   m.PartitionId as partitionID, 
	   'Person' as Class,
	   'Custom25' as FieldName,
	   'Healthways Number' as Label,
	   'Text' as Type,
	   '4' as sort,
	   NULL as params,
	   '1' as IsImportant,
	   'Custom' as Location
From (select partitionID,schoolname 
	  from Partition 
	  where (ParentPartitionId = @BrickhouseParentPartitionID or partitionID = @BrickhouseParentPartitionID)
			and PartitionId <> @PartitionThatHasCustomFields ) m
*/			
			
	   	 	
		