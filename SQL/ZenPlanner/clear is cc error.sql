update CreditCard 
set isError = 0
where CreditCardId in 
						(
						select distinct cc.CreditCardId -- prtn.schoolname, pn.firstname, pn.lastname, pt.friendlyid, cc.Label, cc.isError 
						from Payment pt 
								inner join 
										(
										select distinct PartitionId 
										from objectproperty op 
										where cast(op.PropertyValue as varchar(250)) in ('AuthorizeNet_EFT', 'AuthorizeNet_CC')		
										) M
										on M.PartitionId = pt.PartitionId  
								inner join Partition prtn on prtn.PartitionId =  pt.PartitionId 
								inner join Person pn on pn.PersonId = pt.PersonId 
								inner join CreditCard cc on cc.CreditCardId = pt.CreditCardId 

						where pt.Status = 'ERROR'
								and pt.PaymentDate >= '2014-10-08 00:00:00'
								and isError = '1'
						)