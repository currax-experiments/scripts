/*
	create table TableMaintenance
	(
		id						 int not null identity(1,1),
		DatabaseName			 varchar(100),
		DatabaseSchema			 varchar(100),
		TableName				 varchar(100),
		DescriptionOfMaintenance varchar(200),
		MaintenanceAction		 varchar(50),
		Command					 varchar(max),
		Start					 smalldatetime,
		Complete				 smalldatetime,
		RunID					 uniqueidentifier,
	)

	ALTER TABLE TableMaintenance
	ADD CONSTRAINT PK_TableMaintenance PRIMARY KEY(ID)
*/

/*
Declare @RunID uniqueidentifier = '3381F70B-1123-4014-A7F6-11F508905035'

select * from TableMaintenance 
where start is not null and complete is null and runID = @RunID

select count(*) from TableMaintenance where start is not null and complete is not null and runID = @RunID
select count(*) from TableMaintenance where start is null and complete is null and runID = @RunID

*/

DECLARE @RUNID uniqueidentifier = newid()
DECLARE @SQLCommand varchar(max) = ''
DECLARE @ID_Cursor INT = 0
DECLARE @TableName_Cursor varchar(max) = ''
DECLARE @DatabaseName_Cursor varchar(100) = ''
DECLARE @DatabaseSchema_Cursor varchar(100) = ''
DECLARE @WITHFULLSCAN BOOLEAN = 'FALSE'
DECLARE @RUNTYPE varchar(50) = 'RUN'

Insert Into [ZenPlanner-Management].dbo.TableMaintenance
	(DatabaseName,DatabaseSchema,TableName,MaintenanceAction,RunID)
Select   
	Table_Catalog,TABLE_SCHEMA,TABLE_NAME,'Statistics Update',@RUNID
From INFORMATION_SCHEMA.tables 
Where TABLE_TYPE = 'BASE TABLE'
		and TABLE_NAME NOT LIKE '[__]%'
		and TABLE_NAME NOT LIKE '%[_]%'


IF CURSOR_STATUS('global','MaintenanceCursor') >= -1
BEGIN
	DEALLOCATE MaintenanceCursor
END

DECLARE MaintenanceCursor CURSOR for 
									Select ID, Databasename,DatabaseSchema,TableName
									From [Zenplanner-Management].dbo.TableMaintenance 
									Where RunID = @RUNID

OPEN MaintenanceCursor
FETCH NEXT FROM MaintenanceCursor into @ID_Cursor, @DatabaseName_Cursor, @DatabaseSchema_Cursor, @TableName_Cursor
WHILE @@FETCH_STATUS = 0
BEGIN
	-- Set the command
	SET @SQLCommand = 'USE [' + @DatabaseName_Cursor + ']; UPDATE STATISTICS ' + @DatabaseSchema_cursor + '.[' + @TableName_Cursor + ']'
	IF @WITHFULLSCAN
	BEGIN
		SET @SQLCommand = @SQLCommand + ' WITH FULLSCAN'
	END 
	
	
	-- Set the Start Time
	update [Zenplanner-Management].dbo.TableMaintenance
	Set Start = cast(getdate() as smalldatetime), command = @SQLCommand
	where id = @ID_Cursor 

	-- Print Command
	print @SQLCommand
	
	-- Run the Command
	IF @RUNTYPE = 'RUN'
	BEGIN
		exec(@SQLCommand)
	END

	-- Set the completed datetime 
	update [Zenplanner-Management].dbo.TableMaintenance
	Set Complete = cast(getdate() as smalldatetime)
	where id = @ID_Cursor 


	FETCH NEXT FROM MaintenanceCursor into @ID_Cursor, @DatabaseName_Cursor, @DatabaseSchema_Cursor, @TableName_Cursor
END


-- truncate table [zenplanner-management].dbo.TableMaintenance
-- select * from [zenplanner-management].dbo.TableMaintenance