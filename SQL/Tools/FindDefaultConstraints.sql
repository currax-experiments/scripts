select c.name from 
sys.all_columns a 
inner join
sys.tables b 
on 
a.object_id = b.object_id
inner join
sys.default_constraints c
on a.default_object_id = c.object_id
where b.name='tablename'
and a.name = 'columnname'