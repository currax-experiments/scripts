--dbcc opentran

SELECT ec.session_id, ec.connect_time, tst.is_user_transaction, st.text, es.host_name 
	FROM sys.dm_tran_session_transactions tst 
		INNER JOIN sys.dm_exec_connections ec ON tst.session_id = ec.session_id
		inner join sys.dm_exec_sessions es on es.session_id = tst.session_id 
		CROSS APPLY sys.dm_exec_sql_text(ec.most_recent_sql_handle) st