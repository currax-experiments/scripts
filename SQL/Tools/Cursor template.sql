IF CURSOR_STATUS('global','<CURSOR NAME YOU ARE CREATING>')> = -1
	BEGIN
	 DEALLOCATE <CURSOR NAME YOU ARE CREATING>
	END	
	
DECLARE <CURSOR NAME YOU ARE CREATING> CURSOR FOR <Some select statement to get your data>
OPEN <CURSOR NAME YOU ARE CREATING>

Fetch Next From <CURSOR NAME YOU ARE CREATING> into <List of variables>
while @@FETCH_STATUS = 0
Begin 
-- whatever you are going to do

END
	