declare @SourceDatabase varchar(200) = '[CreditCardImport]'
declare @DestinationDatabase varchar(200) = 'CreditCardImport_restore'
declare @DynamicSQL varchar(MAX) = ''
declare @TABLE_NAME_CURSOR varchar(200) = ''

use [CreditCardImport_restore] 
EXEC sp_msforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"


use [CreditCardImport]

IF CURSOR_STATUS ('global', 'TableListCursor')>=-1
BEGIN
 DEALLOCATE TableListCursor
END

DECLARE TableListCursor CURSOR Static FOR 
	select TABLE_NAME from INFORMATION_SCHEMA.tables order by TABLE_NAME asc
OPEN TableListCursor
FETCH NEXT FROM TableListCursor INTO @Table_Name_Cursor  

WHILE @@FETCH_STATUS = 0  
BEGIN  
       FETCH NEXT FROM TableListCursor INTO @TABLE_NAME_CURSOR 
	   SET @DynamicSQL = 'Truncate Table [CreditCardImport_Restore].dbo.[' + @Table_Name_cursor + ']; Insert into [CreditCardImport_Restore].dbo.[' + @TABLE_NAME_CURSOR + '] Select * from [CreditCardImport].dbo.[' + @TABLE_NAME_CURSOR  + ']'
	   exec(@DynamicSQL)
	   Print @DynamicSQL
	   -- Print @Table_Name_cursor
END  

CLOSE TableListCursor
DEALLOCATE TableListCursor


use [CreditCardImport_restore] 
exec sp_msforeachtable @command1="print '?'", @command2="ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"



