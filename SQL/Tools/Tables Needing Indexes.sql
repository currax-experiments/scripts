select d.*
        , s.avg_total_user_cost
        , s.avg_user_impact
        , s.last_user_seek
        ,s.unique_compiles
from sys.dm_db_missing_index_group_stats s
		inner join sys.dm_db_missing_index_groups g on s.group_handle = g.index_group_handle
		inner join sys.dm_db_missing_index_details d on d.index_handle = g.index_handle

order by s.avg_user_impact desc