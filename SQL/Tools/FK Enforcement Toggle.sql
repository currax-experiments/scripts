declare @SQLCMD varchar(5000) = ''
declare @EnableDisable varchar(10) = '' -- Enable Disable 

-- Select * from  #FKToggle 

If @EnableDisable = 'Disable'
BEGIN
	If OBJECT_ID('tempdb..#FKToggle') IS NOT NULL
		Drop Table #FKToggle

	Create table #FKToggle
					(TableName Varchar(500),
					 FKName Varchar(500)
					 )
	Insert into #FKToggle 
	select object_name(parent_object_id) as TableName,
			name as FKName
	from sys.foreign_keys sfk
	where is_disabled = '0'
	order by  object_name(parent_object_id)

	-- Disable the foreign Keys 
	IF CURSOR_STATUS('global','fk_Disable_cursor')>=-1
	BEGIN
	 DEALLOCATE FK_Disable_Cursor
	END

	declare FK_Disable_Cursor cursor for 
								select 'Alter Table [' + TableName + '] NOCHECK CONSTRAINT [' + FKName + ']' as SQLCMD
								from #FKToggle 

	open FK_Disable_Cursor
	Fetch next from FK_Disable_Cursor into @SQLCMD
	WHILE @@FETCH_STATUS = 0
	BEGIN
		PRINT @SQLCMD
		exec(@SQLCMD)
		Fetch next from FK_Disable_Cursor into @SQLCMD
END

CLOSE FK_Disable_Cursor
DEALLOCATE FK_Disable_Cursor
END

If @EnableDisable = 'Enable'
BEGIN
	-- Enable the Foreign Keys
	IF CURSOR_STATUS('global','fk_Enable_cursor')>=-1
	BEGIN
	 DEALLOCATE FK_Enable_Cursor
	END

	declare FK_Enable_Cursor cursor for 
								select 'Alter Table [' + TableName + '] CHECK CONSTRAINT [' + FKName + ']' as SQLCMD
								from #FKToggle
								
	open FK_Enable_Cursor
	Fetch next from FK_Enable_Cursor into @SQLCMD
	WHILE @@FETCH_STATUS = 0
	BEGIN
		PRINT @SQLCMD
		exec(@SQLCMD)
		Fetch next from FK_Enable_Cursor into @SQLCMD				
END
If OBJECT_ID('tempdb..#FKToggle') IS NOT NULL
		Drop Table #FKToggle

CLOSE FK_Enable_Cursor
DEALLOCATE FK_Enable_Cursor
END




-- ALTER TABLE MyTable CHECK CONSTRAINT MyConstraint