
SELECT tst.session_id, text, tst.is_user_transaction, ses.host_name 
FROM sys.dm_tran_session_transactions tst 
		INNER JOIN sys.dm_exec_connections ec ON tst.session_id = ec.session_id
		inner join sys.dm_exec_sessions ses on ses.session_id = ec.session_id 
		CROSS APPLY sys.dm_exec_sql_text (ec.most_recent_sql_handle)
where tst.session_ID not in (
								SELECT req.session_id
								FROM sys.dm_exec_requests req
									inner join sys.dm_exec_sessions ses on ses.session_id = req.session_id 
								CROSS APPLY sys.dm_exec_sql_text(sql_handle) AS sqltext
								where text not like 'select sqltext%'
								)

order by tst.session_id asc	