DECLARE @Database VARCHAR(255)   
DECLARE @Table VARCHAR(255)  
DECLARE @cmd NVARCHAR(500)  
DECLARE @fillfactor INT 
DECLARE @EventMessage VARCHAR(255)

SET @fillfactor = 90 

-- Cursor for databases
set @EventMessage = 'Database Index Rebuild Starting'
xp_logevent 999, @EventMessage, informational

DECLARE DatabaseCursor CURSOR FOR  
SELECT name FROM MASTER.dbo.sysdatabases   
WHERE name NOT IN ('master','msdb','tempdb','model','distribution')   
ORDER BY 1  

-- Loop over list of databases
OPEN DatabaseCursor  
FETCH NEXT FROM DatabaseCursor INTO @Database  
WHILE @@FETCH_STATUS = 0  
BEGIN  
	set @EventMessage = 'Database Index Rebuild for database ' + @database + 'Starting'
	xp_logevent 999, @EventMessage, informational
	
	SET @cmd = 'DECLARE TableCursor CURSOR FOR 
				SELECT ''['' + table_catalog + ''].['' + table_schema + ''].['' + table_name + '']'' as tableName
				FROM [' + @Database + '].INFORMATION_SCHEMA.TABLES 
				WHERE table_type = ''BASE TABLE'''   
	
	-- create table cursor  
	EXEC (@cmd)  
	
	-- Loop over list of tables
	OPEN TableCursor   
	FETCH NEXT FROM TableCursor INTO @Table   
	WHILE @@FETCH_STATUS = 0   
		BEGIN   
		   set @EventMessage = 'Index Rebuild for ' + @Table + ' Starting'
		   xp_logevent 999, @EventMessage, informational
		   SET @cmd = 'ALTER INDEX ALL ON ' + @Table + ' REBUILD WITH (FILLFACTOR = ' + CONVERT(VARCHAR(3),@fillfactor) + ')' 
           EXEC (@cmd) 
		   set @EVentMessage = 'Index Rebuild for ' + @Table + ' Completed'
		   xp_logevent 999, @EventMessage, informational
		END
		FETCH NEXT FROM TableCursor INTO @Table   
	END   

   CLOSE TableCursor   
   DEALLOCATE TableCursor  

   FETCH NEXT FROM DatabaseCursor INTO @Database  
END  
CLOSE DatabaseCursor   
DEALLOCATE DatabaseCursor
set @EventMessage = 'Database Index Rebuild Completed'
xp_logevent 999, @EventMessage, informational
