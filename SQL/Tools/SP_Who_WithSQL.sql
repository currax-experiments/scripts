declare @Output table (
						spid int
						,[status] varchar(200)
						,[login]  varchar(200)
						,hostname varchar(200)
						,blkBy	varchar(200)
						,DBName	varchar(200)
						,Command varchar(200)
						,CPUTime bigint
						,DiskIO  bigint
						,lastbatch	varchar(1000)
						,programName	varchar(200)
						,spid2	int
						,requestID	int
						)
insert into @Output
exec sp_who2

select o.login, o.hostname, sqltext.text,	o.lastbatch  
from sys.sysprocesses sp 
		inner join @Output o on o.spid = sp.spid 
		OUTER APPLY sys.dm_exec_sql_text(sql_handle) sqltext