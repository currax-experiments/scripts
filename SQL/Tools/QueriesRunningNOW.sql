SELECT sqltext.TEXT,
req.session_id,
req.status,
req.command,
req.cpu_time,
req.total_elapsed_time,
ses.host_name,
db_name(req.database_id) as databaseName
FROM sys.dm_exec_requests req
	inner join sys.dm_exec_sessions ses on ses.session_id = req.session_id 
CROSS APPLY sys.dm_exec_sql_text(sql_handle) AS sqltext
where text not like 'select sqltext%'
order by total_elapsed_time desc