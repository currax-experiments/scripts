-- Update the day on the agents stats
================================================================================
declare @TimeWeAreSetting datetime

Set @TimeWeAreSetting = '10/15/2012 00:00:00'

use localwork 
update tmp_agent_statistics 
set timestamp = @TimeWeAreSetting 
where timestamp is NULL

select *
from tmp_agent_statistics where timestamp = @TimeWeAreSetting 


  
-- Import all group stats from temp import table
================================================================================
use localwork 

delete from CallCenterAgentStatistics

insert into CallCenterAgentStatistics (LogDate,AgentName, NumberCallshandled,
			NumberCallsUnanswered,AverageCallTime,TotalTalkTime,TotalStaffedTime,CallCenter)
select 
	timestamp,
	[agent name],
	cast([num  calls handled ] as int),
	CAST([num  calls unanswered ] as int),
	dbo.ConvertHHMMSS2Seconds([avg  call time ]),
	dbo.ConvertHHMMSS2Seconds([total talk time ]),
	dbo.ConvertHHMMSS2Seconds([total staffed time ]),
	CallCenter
from tmp_agent_statistics 



-- Import all group stats from temp import table
=================================================================================
use localwork 
Delete from CallCenterGroupStatistics

insert into CallCenterGroupStatistics 
	(LogDate, NumberBusyOverflows,NumCallsAnswered,NumberCallsAbandoned,
	 NumberCallsTransferred,NumberCallsTimedOut,AverageNumberAgentsTalking,
	 AverageNumberAgentsStaffed,AverageWaitTime,AverageAbandonmentTime,CallCenter)

select 
   timestamp,
   cast([num  busy overflows] as int),
   cast([num  calls answered] as int),
   cast([num  calls abandoned ] as int),
   cast([num  calls transferred ] as int),
   cast([num  calls timed out ] as int),
   cast([avg  num  agents talking ] as float),
   cast([avg  num  agents staffed ] as float),
   dbo.ConvertHHMMSS2Seconds([avg  wait time ]),
   dbo.ConvertHHMMSS2Seconds([avg  abandonment time]),
   CallCenter
from tmp_group_statistics


-- Total Calls By Day Of Week (For next week, we may want an hourly breakdown)
==================================================================================
   

-- Avearge - Shortest call times
=================================================================================
use localwork
Declare @MondayOfWeek datetime = '10/15/2012 00:00:00'
select AgentName, 
	   cast(((agent_statistics.AverageCallTime % 3600)/60) as varchar(max)) + ':' + cast((agent_statistics.AverageCallTime % 60) as varchar(max)) as CallTime,             
       convert(varchar, agent_statistics.LogDate, 110) as Date,
	   CallCenter
from agent_Statistics
		inner join (select distinct max(averagecalltime) as AverageCallTime,
					cast(logdate as DATE) as LogDate
					from agent_Statistics 
					Where AverageCallTime <> '0'
						  and logdate >=  @MondayOfWeek and logdate <= DATEADD(DAY,5,@MondayOfWeek)
					group by LogDate 
					) sc
				on (agent_statistics.LogDate = sc.LogDate AND agent_statistics.AverageCallTime = sc.averagecalltime)


-- Handled Most Calls
=================================================================================
use localwork
Declare @MondayOfWeek datetime = '10/15/2012 00:00:00'
select AgentName, agent_statistics.NumberCallsHandled, agent_statistics.LogDate, CallCenter
from agent_Statistics
		inner join (select distinct max(NumberCallsHandled) as NumberCallsHandled,
					cast(logdate as DATE) as LogDate
					from agent_Statistics 
					Where NumberCallshandled <> '0'
					group by LogDate 
					) sc
				on (agent_statistics.LogDate = sc.LogDate AND agent_statistics.NumberCallsHandled = sc.NumberCallsHandled)
where sc.LogDate >=  @MondayOfWeek and sc.LogDate <= DATEADD(DAY,5,@MondayOfWeek)
		
		