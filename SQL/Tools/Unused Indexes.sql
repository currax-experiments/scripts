/*
Script to check for indexes that have a greater write to read ratio and have 
been updated in the last week. Original script by Glenn Berry Modified 
by Richard Douglas 
		Removed Primary Keys from resultset 
		Removed unique indices from resultset
		Added a date range for the query. 
		Added a query to show server uptime to add quantification to the figures. 
		Added [SpaceUsed KB] attribute 
*/

Read more: T-SQL Tuesday #10: Unused Indexes (Indices) | Richard Douglas - SQL Server Professional http://sql.richarddouglas.co.uk/archive/2010/09/t-sql-tuesday-10-unused-indexes-indices.html#ixzz291sBaQ00

SELECT Create_Date [UpSince], DATEDIFF(DD,Create_Date, GETDATE()) [UpTimeInDays] 
FROM sys.databases WHERE name = 'tempdb'; 

go 

DECLARE @Today DATETIME,
		@LastWeek DATETIME; 

SELECT @Today = GETDATE(), @LastWeek = DATEADD(DD,-7,@Today); 

SELECT DB_Name() [DBName], 
	   OBJECT_NAME(s.[object_id]) AS [Table Name],
	   i.name AS [Index Name],
	   i.index_id,
	   user_updates AS [Total Writes],
	   user_seeks + user_scans + user_lookups AS [Total Reads],
	   user_updates - (user_seeks + user_scans + user_lookups) AS [Difference],
	   ISNULL((select 8192 * SUM(a.used_pages - CASE WHEN a.type <> 1 THEN a.used_pages WHEN p.index_id < 2 THEN a.data_pages ELSE 0 END) 	   
			   FROM sys.partitions as p JOIN sys.allocation_units as a ON a.container_id = p.partition_id 
			   WHERE p.object_id = i.object_id AND p.index_id = i.index_id)/1024 ,0.0) AS [SpaceUsed KB] 
			   
FROM sys.dm_db_index_usage_stats AS s WITH (NOLOCK) 
		INNER JOIN sys.indexes AS i WITH (NOLOCK) ON s.[object_id] = i.[object_id] 
					AND i.index_id = s.index_id WHERE OBJECTPROPERTY(s.[object_id],'IsUserTable') = 1 
					AND s.database_id = DB_ID() AND user_updates > (user_seeks + user_scans + user_lookups) 
					AND i.index_id > 1 AND user_seeks + user_scans + user_lookups = 0 
					AND Last_User_Update BETWEEN @LastWeek AND @Today 
					AND Is_Primary_Key = 0 
					AND IS_Unique = 0 

ORDER BY [Difference] DESC, [Total Writes] DESC, [Total Reads] ASC;

/*
===================================================================================================================================================
===================================================================================================================================================
*/

WITH cte AS (
    SELECT 
    	'['+ c.name + '].[' + o.name + ']' AS TableName,
    	i.name AS IndexName,
    	i.index_id AS IndexID,   
    	user_seeks + user_scans + user_lookups AS Reads,
    	user_updates AS Writes,
    	(
    		SELECT SUM(p.rows) 
    		FROM sys.partitions p 
    		WHERE p.index_id = s.index_id 
    			AND s.object_id = p.object_id
    	) AS TotalRows,
    	CASE
    		WHEN s.user_updates < 1 THEN 100
    		ELSE 1.00 * (s.user_seeks + s.user_scans + s.user_lookups) 
    			/ s.user_updates
    	END AS ReadsPerWrite,
     
    	ISNULL((select 8192 * SUM(a.used_pages - CASE WHEN a.type <> 1 THEN a.used_pages WHEN p.index_id < 2 THEN a.data_pages ELSE 0 END) 	   
			   FROM sys.partitions as p JOIN sys.allocation_units as a ON a.container_id = p.partition_id 
			   WHERE p.object_id = i.object_id AND p.index_id = i.index_id)/1024 ,0.0) AS [SpaceUsed KB],
    	
    	'DROP INDEX ' + QUOTENAME(i.name) 
    		+ ' ON ' + QUOTENAME(c.name) 
    		+ '.' + QUOTENAME(OBJECT_NAME(s.object_id)) 
    		AS 'DropSQL'
    FROM sys.dm_db_index_usage_stats s  
    INNER JOIN sys.indexes i ON i.index_id = s.index_id 
    	AND s.object_id = i.object_id   
    INNER JOIN sys.objects o on s.object_id = o.object_id
    INNER JOIN sys.schemas c on o.schema_id = c.schema_id
    WHERE OBJECTPROPERTY(s.object_id,'IsUserTable') = 1
    	AND s.database_id = DB_ID()   
    	AND i.type_desc = 'nonclustered'
    	AND i.is_primary_key = 0
    	AND i.is_unique_constraint = 0
    AND 
    (
    	SELECT SUM(p.rows) 
    	FROM sys.partitions p 
    	WHERE p.index_id = s.index_id 
    		AND s.object_id = p.object_id
    ) > 10000
)
SELECT * 
FROM cte
where ReadsPerWrite < 1 order by ReadsPerWrite ASC

