GO
IF NOT EXISTS ( SELECT  *
                FROM    [sys].[tables]
                WHERE   [name] = N'DatabaseSize'
                        AND [type] = N'U' ) 
    CREATE TABLE [dbo].[DatabaseSize]
        (
          [ID] [BIGINT] IDENTITY(1, 1) ,
          [CaptureDate] [DATETIME] ,
          [DatabaseName] [NVARCHAR](120) ,
		  [LogicalFileName] [NVARCHAR](120) ,
		  [PhysicalName] [NVARCHAR](300) ,
          [SizeMB] [DECIMAL](14, 2) ,
          [FreeSpaceMB] [DECIMAL](14, 2),
		  [FileType] varchar(30)
        );
GO

/* ============================================================================================= */

GO
if exists (
		select  * from tempdb.dbo.sysobjects o
		where o.xtype in ('U')  and o.id = object_id(N'tempdb..#tmp_dbsize')
	)
	DROP TABLE #tmp_dbsize 

Create Table #tmp_dbsize
(
    DatabaseName sysname,
    Name sysname,
    physical_name nvarchar(500),
    size decimal (18,2),
    FreeSpace decimal (18,2),
	type tinyint
)   
Exec sp_msforeachdb '
Use [?];
Insert Into #tmp_dbsize (DatabaseName, Name, physical_name, Size, FreeSpace,type)
    Select DB_NAME() AS [DatabaseName], Name,  physical_name,
    Cast(Cast(Round(cast(size as decimal) * 8.0/1024.0,2) as decimal(18,2)) as nvarchar) Size,
    Cast(Cast(Round(cast(size as decimal) * 8.0/1024.0,2) as decimal(18,2)) -
        Cast(FILEPROPERTY(name, ''SpaceUsed'') * 8.0/1024.0 as decimal(18,2)) as nvarchar) As FreeSpace,
		type
    From sys.database_files
'
insert into [zenplanner-manage].dbo.DatabaseSize
	(CaptureDate,DatabaseName,LogicalFileName,Physicalname,SizeMB,FreeSpaceMB)
Values	
	Select getdate() as captureDate,
		DatabaseName,
		Name,
		physical_name,
		size,
		freespace,
		fileType =
			case
				when type = 0 then 'Rows'
				when type = 1 then 'Log'
				when type = 2 then 'FileStream'
				when type = 3  then 'NotSupported'
				when type = 4 then 'FullText'
			end
	From #tmp_dbsize

drop table #tmp_dbsize