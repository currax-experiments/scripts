/* Message Queue Current Usage */

select type, COUNT(*)
from MessageQueue 
where lockId is null and retryCount > 0
group by type 

go

select distinct type, priority from MessageQueue order by priority asc

go

select COUNT(*) as number_of_items_in_queue
from MessageQueue  
where retryCount > '0' and lockId IS NULL -- and type='MembershipRefresh'

