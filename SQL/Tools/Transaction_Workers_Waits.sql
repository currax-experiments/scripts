SELECT sqltext.TEXT,
req.session_id,
req.status,
req.command,
req.cpu_time,
req.total_elapsed_time,
ses.host_name,
db_name(req.database_id) as databaseName
FROM sys.dm_exec_requests req
	inner join sys.dm_exec_sessions ses on ses.session_id = req.session_id 
CROSS APPLY sys.dm_exec_sql_text(sql_handle) AS sqltext
where text not like 'select sqltext%'
order by session_id asc, total_elapsed_time desc



SELECT dm_ws.wait_duration_ms,
dm_ws.wait_type,
dm_es.status,
dm_t.TEXT,
dm_qp.query_plan,
dm_ws.session_ID,
dm_es.cpu_time,
dm_es.memory_usage,
dm_es.logical_reads,
dm_es.total_elapsed_time,
dm_es.program_name,
DB_NAME(dm_r.database_id) DatabaseName,
-- Optional columns
dm_ws.blocking_session_id,
dm_r.wait_resource,
dm_es.login_name,
dm_r.command,
dm_r.last_wait_type
FROM sys.dm_os_waiting_tasks dm_ws
INNER JOIN sys.dm_exec_requests dm_r ON dm_ws.session_id = dm_r.session_id
INNER JOIN sys.dm_exec_sessions dm_es ON dm_es.session_id = dm_r.session_id
CROSS APPLY sys.dm_exec_sql_text (dm_r.sql_handle) dm_t
CROSS APPLY sys.dm_exec_query_plan (dm_r.plan_handle) dm_qp
WHERE dm_es.is_user_process = 1
order by dm_ws.session_id asc
GO


SELECT tst.session_id, text, tst.is_user_transaction, ses.host_name 
FROM sys.dm_tran_session_transactions tst 
		INNER JOIN sys.dm_exec_connections ec ON tst.session_id = ec.session_id
		inner join sys.dm_exec_sessions ses on ses.session_id = ec.session_id 
		CROSS APPLY sys.dm_exec_sql_text (ec.most_recent_sql_handle)
order by tst.session_id asc		