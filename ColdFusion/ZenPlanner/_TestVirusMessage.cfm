component extends="BaseMessageHandler"
{
	public void function onMessage(required string message)
	{	
		if (RandRange(1,10) LE 4)
		{
			Application.q.sendMessage(type='_TestVirusMessage', data={message=CreateUUID()});
		}
		local.x = RandRange(1,50)
		
		if ((local.x GE 1) and (local.x LE 10))
		{sleep(1000); }
		
		if ((local.x GE 11) and (local.x LE 20))
		{sleep(5000); }
		
		if ((local.x GE 21) and (local.x LE 30))
		{sleep(10000); }

		if ((local.x GE 31) and (local.x LE 40))
		{sleep(15000); }
		
		if ((local.x GE 41) and (local.x LE 50))
		{sleep(20000); }
		
		WriteLog (type="Info",file="ZenVirusQueue.log",Text="Running message: #Arguments.message#, sleep time #local.x#"  )
	}
}

<!---
use [ZenPlanner-Development] 
Truncate table MessageQueue
Truncate table MessageQueueHistory

declare @x int = 1

while (@x <= 100000)
	begin
		insert into MessageQueue 
			(messageQueueId, type, data, priority,beginDate, retryCount)
			values
			(NEWID(),'_TestVirusMessage','{''Message'':''Running Test MessageQueue''}','10',GETDATE(),'3')
		
		set @x = @x + 1
	end
--->