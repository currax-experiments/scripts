-- {'MessageID':'DCA05FCB-B07F-4086-AB3A-FDDE0349CBBD'}

DECLARE @i int = '1'
Declare @LoopEnd int = '100000'
Declare @MessageID uniqueidentifier 
Declare @DataMessage varchar(100) = ''

WHILE (@i <= @LoopEnd)
BEGIN
	set @MessageID = newid()
	set @DataMessage = '{''MessageID'':'''+convert(varchar(45),@MessageID)+'''}'
	Print @DataMessage
	insert into MessageQueue
		(messageQueueID, type, data,priority,begindate,retrycount)
	values
		(@MessageID,'TestDuplicate',@DataMessage,'1',getdate(),'1')
	set @i = @i + 1
	print @i
END	