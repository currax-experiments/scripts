/* Create the Table */
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'FriendlyIDCounter' )
Begin
	create table FriendlyIDCounter
		(
		FriendlyIDCounterID uniqueidentifier not null,
		partitionID uniqueidentifier not null,
		CounterClass varchar(30) not null,
		counter bigint default 1 not null
		)
End

/* Create the index */
GO
IF EXISTS (SELECT name FROM sys.indexes
            WHERE name = N'IX_PartitionID_CounterClass') 
    DROP INDEX IX_partitionID_CounterClass ON FriendlyIDCounter; 
GO

CREATE NONCLUSTERED INDEX IX_PartitionID_CounterClass 
    ON FriendlyIDCounter (PartitionID, CounterClass);

/* Get the next ID */	
declare @SchoolPartitionID uniqueidentifier = 'FC2D25F4-9DD4-4AE4-9910-CF078C66CD86'
declare @CounterClass Varchar(30) = 'Bills'

update FriendlyIDCounter
set counter = counter + 1
output inserted.counter
where partitionID = @SchoolPartitionID and CounterClass = @CounterClass
if @@rowcount = 0
	begin
		insert into FriendlyIDCounter
			(FriendlyIdCounterID,PartitionID,CounterClass)
			output inserted.counter
		values
			(newID(),@SchoolPartitionID,@CounterClass)
		
	end

