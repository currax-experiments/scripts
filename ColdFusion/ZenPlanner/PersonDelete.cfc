component extends="BaseMessageHandler"
{
	public void function onMessage(required guid partitionId, required guid personId)
	{
		local.database = Application.db.getLocalDatabase(Arguments.partitionId);
		
		local.person = local.database.load('Person', Arguments.personID);
		local.person.delete();
	}
}